# client
This is the client that is used for the bot instances. It accepts
commands over gRPC. You can find the service spec [here](./schema/service.proto).

## Contents
- [Clustering](#clustering)
- [Environment Variables](#environment-variables)

## Clustering
The client is unlike standard bot applications. Rather than control
a single bot instance, it controls multiple, each with certain weights
that fluctuate over time. The weighting scheme can allow a cluster of
clients to tightly pack bots into application instances and dynamically
scale when necessary.

### Leader
The cluster has one leader. It is responsible for queueing bots for assignment
as well as matching requests for specific bots to the correct clients. The
leader does not contain any bots itself, so the minimum cluster size is two.

Services that interact with the cluster should first determine which client
is the leader and then send all subsequent requests to that destination. If
the leader fails, another client will take on the role.

### Follower
Any client that is not the leader is a follower. These instances contain
running bots that are manipulated by requests from the leader.

## Environment Variables
- ZOOKEEPER_SERVERS
    - A comma-separated list of ZooKeeper server addresses
- MAX_WEIGHT
    - The maximum weight the client can handle
- RPC_PORT
    - The port where gRPC requests are handled. Defaults to 8080
- PING_PORT
    - The port where HTTP ping requests are handled. Defaults to 8085