syntax = "proto3";

package io.botbin.client.event.serial;
option go_package = "gitlab.com/botbin/client/pkg/event/serial";

// An Event describes the state of a context at a single point in time.
// There is no restriction on whether the event must have occurred in
// the past. It may describe an expected future state that is left to
// some service to materialize.
message Event {
    // A Unix timestamp
    int32 timestamp = 1;

    oneof state {
        GuildMessage guild_message = 2;
        Message direct_message = 3;
    }
}

// A GuildMessage is a message that occurred in a guild, as opposed to
// being a direct message.
message GuildMessage {
    NamedEntity guild = 1;
    Message message = 2;
}

// A Message defines a text-based event that was created by some
// uniquely-identifiable entity.
message Message {
    NamedEntity channel = 1;
    Author author = 2;
    uint64 id = 3;
    string content = 4;
}

// An Author is a named entity that is capable of producing events.
// These types may refer to real users or bots.
message Author {
    NamedEntity identity = 1;
}

// A NamedEntity is thing that has a unique ID within its context and
// possibly globally. These types are accompanied by a name that refers
// to the ID but that may not be unique itself.
message NamedEntity {
    uint64 id = 1;
    string name = 2;
}
