PROJECT_NAME := client
# Base source path for protobuf includes
PROTO_INC := $(if $(GOROOT),$(GOROOT),$(HOME)/go)/src
COMPOSE_TEST_FILE := docker/docker-compose.test.yaml

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${PROJECT_NAME} cmd/main.go

test:
	go test -coverprofile=cov.out ./pkg/...

test_all:
	docker-compose -f ${COMPOSE_TEST_FILE} down -v || true
	docker-compose -f ${COMPOSE_TEST_FILE} up --build --abort-on-container-exit
	docker-compose -f ${COMPOSE_TEST_FILE} down -v

proto:
	protoc -I ${PROTO_INC} --go_out=plugins=grpc:${PROTO_INC} ${PROTO_INC}/gitlab.com/botbin/${PROJECT_NAME}/schema/service.proto
	protoc -I ${PROTO_INC} --go_out=${PROTO_INC} ${PROTO_INC}/gitlab.com/botbin/${PROJECT_NAME}/schema/event.proto

version:
	$(eval VERSION=$(shell ./version.sh))

image: version
	docker build -f docker/Dockerfile -t ${REGISTRY}/${PROJECT_NAME}:${VERSION} .

deploy_image: image
	docker push ${REGISTRY}/${PROJECT_NAME}:${VERSION}

.PHONY: test image