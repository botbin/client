# botconf
This package parses configurations for bot clients.

## Version 1 Format
A complete configuration will have the following shape:
```yaml
version: 1
metadata:
  name: pingaroo
  version: 0.1.0
spec:
  trigger: "."
  modules:
  - name: ping
    author: tester
    tag: 1.2.3
    scopes:
    - message
    trigger: "ping"
```
In this example, any message prefixed with `.` will be parsed by the
bot. By appending a module trigger to that prefix, we can further
route a message to module processing logic. One might imagine that
sending `.ping` could result in the bot responding with `pong`.

Scopes are an additional property that a module may possess. Their intent
is to give bot owners the authority to choose what types of actions
a module may perform. In the example, a message scope might permit the
ping module to send messages to any channel.

### Optional Fields
The following fields are optional:
- metadata.version
- spec.modules.trigger