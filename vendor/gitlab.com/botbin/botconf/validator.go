package botconf

// Validator types ensure a config is valid for a certain version.
type Validator interface {
	Validate(*BotConfig) []error
}
