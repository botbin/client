package botconf

// ConfigLoader types load bot configurations from text.
type ConfigLoader interface {
	Load([]byte) (*BotConfig, error)
}
