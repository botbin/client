package botconf

import (
	"gopkg.in/yaml.v2"
)

// yamlConfigLoader loads context in the YAML format.
type yamlConfigLoader struct {
}

// Load produces a bot config from YAML text.
func (ycl *yamlConfigLoader) Load(content []byte) (*BotConfig, error) {
	conf := &BotConfig{}
	err := yaml.UnmarshalStrict(content, conf)
	return conf, err
}

// NewYAMLLoader returns a ConfigLoader for YAML text.
func NewYAMLLoader() ConfigLoader {
	return &yamlConfigLoader{}
}
