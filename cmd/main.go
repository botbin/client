package main

import (
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/samuel/go-zookeeper/zk"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/data"

	"gitlab.com/botbin/client/pkg/service"
	"go.uber.org/zap"
)

const version = "0.7.1"

func main() {
	zap.S().Infof("started bot v%s", version)

	ping := new(service.PingServer)
	pingErr := ping.Serve(getPingPort())

	rpc := getServer()
	if err := rpc.Start(); err != nil {
		zap.S().Errorw("gRPC server stopped with non-nil error", "error", err)
	}
	ping.Shutdown(time.Second)

	go func() {
		exitSignal := getExitSignal()
		select {
		case <-exitSignal:
		case err := <-pingErr:
			if err != nil {
				zap.S().Errorw("encountered error with ping server", "error", err)
			}
		}
		ping.Shutdown(time.Second)
		rpc.Stop()
	}()
}

func getServer() *service.DynamicServer {
	zkConn := openZooKeeperConnection()
	state := cluster.NewZKState(zkConn, "/io.botbin/bot-client")
	port := getRPCPort()
	maxWeight := getMaxWeight()

	server, err := service.NewDynamicServer(state, port, maxWeight)
	if err != nil {
		zap.S().Panicw("failed to start server", "error", err)
	}
	return server
}

func openZooKeeperConnection() *zk.Conn {
	zkConn, err := data.GetZK()
	if err != nil {
		zap.S().Panicw("failed to acquire connection to ZooKeeper", "error", err)
	}
	return zkConn
}

func getRPCPort() string {
	port := os.Getenv("RPC_PORT")
	if port == "" {
		port = "8080"
	}
	return port
}

func getPingPort() string {
	port := os.Getenv("PING_PORT")
	if port == "" {
		port = "8085"
	}
	return port
}

func getMaxWeight() float64 {
	request := os.Getenv("MAX_WEIGHT")
	limit, err := strconv.ParseFloat(request, 64)
	if err != nil {
		def := 5000.0
		zap.S().Warnw("invalid value provided for MAX_WEIGHT",
			"value", request,
			"default", def,
		)
		limit = def
	}
	return limit
}

func getExitSignal() <-chan os.Signal {
	exit := make(chan os.Signal)
	signal.Notify(exit, syscall.SIGTERM)
	signal.Notify(exit, syscall.SIGINT)
	return exit
}

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}
