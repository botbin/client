package lead_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"gitlab.com/botbin/client/pkg/service/lead"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster/mocks"
	"gitlab.com/botbin/client/pkg/service/common"
	"gitlab.com/botbin/client/pkg/service/serial"
)

// Tests Server
type ServerTestSuite struct {
	suite.Suite
}

// update when bot location is found
func (ss *ServerTestSuite) TestUpdate_ClientDiscovered() {
	updateRequest := &serial.UpdateRequest{Id: 1, Config: &serial.Config{Content: ""}}

	botAPI := new(mocks.BotAPI)
	client := new(mocks.ClientFacade)
	botAPI.On("GetLocation", updateRequest.GetId()).Return(client, nil)
	client.On("Connect").Return(nil)
	client.On("Disconnect").Return(nil)
	client.On("UpdateBot", mock.Anything).Return(nil)

	clientAPI := new(mocks.ClientAPI)
	state := new(mocks.State)
	state.On("GetBotAPI").Return(botAPI, nil)
	state.On("GetClientAPI").Return(clientAPI, nil)

	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	server, err := lead.New(base, state)
	ss.Nil(err)

	res, err := server.Update(context.Background(), updateRequest)
	ss.Nil(err)
	ss.NotNil(res)

	botAPI.AssertExpectations(ss.T())
	client.AssertExpectations(ss.T())
	state.AssertExpectations(ss.T())
}

// update when bot location can't be found
func (ss *ServerTestSuite) TestUpdate_ClientNotFound() {
	updateRequest := &serial.UpdateRequest{Id: 1, Config: &serial.Config{Content: ""}}

	botAPI := new(mocks.BotAPI)
	botAPI.On("GetLocation", updateRequest.GetId()).Return(nil, errors.New("missing"))

	clientAPI := new(mocks.ClientAPI)
	state := new(mocks.State)
	state.
		On("GetBotAPI").Return(botAPI, nil).
		On("GetClientAPI").Return(clientAPI, nil)

	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	server, err := lead.New(base, state)
	ss.Nil(err)

	_, err = server.Update(context.Background(), updateRequest)
	ss.NotNil(err)

	botAPI.AssertExpectations(ss.T())
	state.AssertExpectations(ss.T())
}

func (ss *ServerTestSuite) TestStop_ClientDiscovered() {
	stopRequest := &serial.StopRequest{Id: 1}

	botAPI := new(mocks.BotAPI)
	client := new(mocks.ClientFacade)
	botAPI.On("GetLocation", stopRequest.GetId()).Return(client, nil)
	client.On("Connect").Return(nil)
	client.On("Disconnect").Return(nil)
	client.On("StopBot", mock.Anything).Return(nil)

	clientAPI := new(mocks.ClientAPI)
	state := new(mocks.State)
	state.
		On("GetBotAPI").Return(botAPI, nil).
		On("GetClientAPI").Return(clientAPI, nil)

	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	server, err := lead.New(base, state)
	ss.Nil(err)

	res, err := server.Stop(context.Background(), stopRequest)
	ss.NotNil(res)
	ss.Nil(err)

	botAPI.AssertExpectations(ss.T())
	client.AssertExpectations(ss.T())
	state.AssertExpectations(ss.T())
}

func (ss *ServerTestSuite) TestStop_ClientNotFound() {
	stopRequest := &serial.StopRequest{Id: 1}

	botAPI := new(mocks.BotAPI)
	botAPI.On("GetLocation", stopRequest.GetId()).Return(nil, errors.New("missing"))

	clientAPI := new(mocks.ClientAPI)
	state := new(mocks.State)
	state.On("GetBotAPI").Return(botAPI, nil)
	state.On("GetClientAPI").Return(clientAPI, nil)

	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	server, err := lead.New(base, state)
	ss.Nil(err)

	_, err = server.Stop(context.Background(), stopRequest)
	ss.NotNil(err)

	botAPI.AssertExpectations(ss.T())
	state.AssertExpectations(ss.T())
}

func (ss *ServerTestSuite) TestGetWeight() {
	state := new(mocks.State)
	state.On("GetBotAPI").Return(new(mocks.BotAPI), nil)
	state.On("GetClientAPI").Return(new(mocks.ClientAPI), nil)

	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	server, err := lead.New(base, state)
	ss.Nil(err)

	res, err := server.GetWeight(context.Background(), new(serial.GetWeightRequest))
	ss.Nil(err)
	ss.Equal(0.0, res.GetWeight())

	state.AssertExpectations(ss.T())
}

func (ss *ServerTestSuite) TestTerminate() {
	state := new(mocks.State)
	state.On("GetBotAPI").Return(new(mocks.BotAPI), nil)
	state.On("GetClientAPI").Return(new(mocks.ClientAPI), nil)

	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	server, err := lead.New(base, state)
	ss.Nil(err)

	go server.Terminate(context.Background(), new(serial.TerminateRequest))

	select {
	case <-base.ErrC:
	case <-time.NewTimer(time.Second).C:
		ss.Fail("timed out")
	}
}

func TestServer(t *testing.T) {
	suite.Run(t, new(ServerTestSuite))
}
