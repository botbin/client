package lead

import (
	"context"
	"encoding/base64"
	"errors"
	"math/rand"
	"time"

	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/service/common"
	"gitlab.com/botbin/client/pkg/service/serial"
	"go.uber.org/zap"
)

// Server implements the common.Server interface for leaders.
type Server struct {
	base *common.ServerBase

	botAPI    cluster.BotAPI
	clientAPI cluster.ClientAPI
}

// New creates a common.Server that performs leader logic.
func New(base *common.ServerBase, state cluster.State) (common.ServerImpl, error) {
	rand.Seed(time.Now().Unix()) // needed for pickRandomClient()

	botAPI, err := state.GetBotAPI()
	if err != nil {
		return nil, err
	}

	clientAPI, err := state.GetClientAPI()
	if err != nil {
		return nil, err
	}

	s := &Server{
		base:      base,
		botAPI:    botAPI,
		clientAPI: clientAPI,
	}
	return s, base.Use(s)
}

// Start assigns the requested bot to an available client.
func (s *Server) Start(ctx context.Context, req *serial.StartRequest) (*serial.StartResponse, error) {
	// TODO: Use metrics to determine the best client to send the bot to.
	// This is just a placeholder method. The entire thing will need to be
	// replaced with the actual leader logic later.

	// TODO: Reject start requests if the bot is already running.
	// This is something to keep in mind.

	client, err := s.pickRandomClient()
	if err != nil {
		return nil, err
	}

	if err := client.Connect(); err != nil {
		zap.S().Error(err)
		return nil, errors.New("no destination client exists for bot")
	}
	defer client.Disconnect()

	bot, err := s.convertStartToBot(req)
	if err != nil {
		return nil, err
	}
	return new(serial.StartResponse), client.StartBot(bot)
}

// picks a random live client that isn't the leader
func (s *Server) pickRandomClient() (cluster.ClientFacade, error) {
	clients, err := s.clientAPI.GetRunningClients()
	if err != nil {
		return nil, err
	}

	start := rand.Intn(len(clients))
	if clients[start].GetAddress() != s.base.Address {
		return clients[0], nil
	}

	for i := (start + 1) % len(clients); i != start; i = (i + 1) % len(clients) {
		if clients[i].GetAddress() != s.base.Address {
			return clients[i], nil
		}
	}
	return nil, errors.New("no destination client exists for bot")
}

func (s *Server) convertStartToBot(req *serial.StartRequest) (*cluster.Bot, error) {
	config, err := s.getConfigContent(req.GetConfig())
	if err != nil {
		return nil, err
	}
	return &cluster.Bot{
		ID:     req.GetId(),
		Config: config,
		Token:  req.GetToken(),
		Weight: req.GetWeight(),
	}, nil
}

// Stop forwards a stop request to the client that owns the specified bot.
func (s *Server) Stop(ctx context.Context, req *serial.StopRequest) (*serial.StopResponse, error) {
	client, err := s.botAPI.GetLocation(req.GetId())
	if err != nil {
		return nil, err
	}

	// TODO: We won't want to keep calling connect every time.
	// The client from GetLocation will have an address before calling connect.
	// If we cache clients, we can use the address to use the already connected
	// client.
	if err := client.Connect(); err != nil {
		return nil, err
	}
	defer client.Disconnect()

	return new(serial.StopResponse), client.StopBot(req.GetId())
}

// Update forwards an update request to the client that owns the specified bot.
func (s *Server) Update(ctx context.Context, req *serial.UpdateRequest) (*serial.UpdateResponse, error) {
	client, err := s.botAPI.GetLocation(req.GetId())
	if err != nil {
		return nil, err
	}

	// TODO: We won't want to keep calling connect every time.
	// The client from GetLocation will have an address before calling connect.
	// If we cache clients, we can use the address to use the already connected
	// client.
	if err := client.Connect(); err != nil {
		return nil, err
	}
	defer client.Disconnect()

	bot, err := s.convertUpdateToBot(req)
	if err != nil {
		return nil, err
	}
	return new(serial.UpdateResponse), client.UpdateBot(bot)
}

func (s *Server) convertUpdateToBot(req *serial.UpdateRequest) (*cluster.Bot, error) {
	config, err := s.getConfigContent(req.GetConfig())
	if err != nil {
		return nil, err
	}
	return &cluster.Bot{
		ID:     req.GetId(),
		Config: config,
	}, nil
}

func (s *Server) getConfigContent(config *serial.Config) (string, error) {
	if config.GetB64Encoded() {
		decoded, err := base64.StdEncoding.DecodeString(config.GetContent())
		return string(decoded), err
	}
	return config.GetContent(), nil
}

// Terminate behaves like ForceTerminate due to the leader not containing bots.
func (s *Server) Terminate(ctx context.Context, req *serial.TerminateRequest) (*serial.TerminateResponse, error) {
	zap.S().Infow("received termination signal", "reason", req.GetReason())
	defer s.base.CloseWithError()
	return new(serial.TerminateResponse), nil
}

// GetWeight returns the current weight of the leader, which is always 0.
func (s *Server) GetWeight(context.Context, *serial.GetWeightRequest) (*serial.GetWeightResponse, error) {
	return &serial.GetWeightResponse{
		Weight: 0,
	}, nil
}

// TearDown gracefully releases acquired resources.
func (s *Server) TearDown() {

}
