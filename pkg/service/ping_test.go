package service_test

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/service"
)

type PingServerTestSuite struct {
	suite.Suite
}

// Ensures that the ping server responds to ping requests.
func (ps *PingServerTestSuite) TestPing() {
	server := new(service.PingServer)
	errC := server.Serve("0")
	time.Sleep(100 * time.Millisecond)

	url := "http://localhost:" + server.Port() + server.Path()
	res, err := http.Get(url)
	ps.Nil(err)
	ps.Equal(http.StatusOK, res.StatusCode)

	timeout := time.Second
	ps.Nil(server.Shutdown(timeout))

	select {
	case <-time.NewTimer(timeout).C:
		ps.Fail("timeout exceeded")

	case err := <-errC:
		ps.Equal(http.ErrServerClosed, err)
	}
}

func TestPingServer(t *testing.T) {
	suite.Run(t, new(PingServerTestSuite))
}
