package service

import (
	"math/rand"
	"time"

	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/metrics"
	"gitlab.com/botbin/client/pkg/service/common"
	"gitlab.com/botbin/client/pkg/service/follow"
	"gitlab.com/botbin/client/pkg/service/lead"
	"gitlab.com/botbin/client/pkg/util"
	"go.uber.org/zap"
)

// DynamicServer controls either a leader or follower server.
type DynamicServer struct {
	base *common.ServerBase

	maxWeight float64
	reporter  metrics.Reporter

	state cluster.State
	port  string
}

// NewDynamicServer creates a DynamicServer instance at the specified port.
func NewDynamicServer(state cluster.State, port string, maxWeight float64) (*DynamicServer, error) {
	base, err := common.NewClusterServerBase(port, state)
	if err != nil {
		return nil, err
	}

	return &DynamicServer{
		base:      base,
		maxWeight: maxWeight,
		state:     state,
		port:      port,
	}, nil
}

// Start opens the server to requests in follower mode.
//
// An error is returned when the server stops. If stopped gracefully, it will be
// nil; otherwise non-nil.
func (ds *DynamicServer) Start() error {
	_, err := follow.New(ds.base, ds.state, ds.maxWeight)
	if err != nil {
		return err
	}

	go ds.handleFutureLeaderRole()
	return <-ds.base.Connect()
}

// prepares the server to switch to a leader role if the situation arises
func (ds *DynamicServer) handleFutureLeaderRole() {
	rand.Seed(time.Now().Unix())
	// Try to even out mass state store traffic if many clients are started at once.
	time.Sleep(time.Millisecond*time.Duration(rand.Intn(1000)) + 1000)

	electionAPI, err := ds.state.GetElectionAPI()
	if err != nil {
		zap.S().Panicw("failed to join leader elections", "error", err)
	}

	addr := cluster.Address(util.GetIP() + ":" + ds.port)
	electionAPI.JoinCandidatePool(addr, ds.assumeLeaderRole)
}

func (ds *DynamicServer) assumeLeaderRole() {
	zap.S().Info("assuming leader role...")

	_, err := lead.New(ds.base, ds.state)
	if err != nil {
		zap.S().Panicw("failed to complete leader process", "error", err)
	}

	zap.S().Info("this client is now the leader")
}

// Stop closes the server.
func (ds *DynamicServer) Stop() {
	ds.base.CloseWithError()
}
