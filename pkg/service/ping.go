package service

import (
	"strings"
	"net"
	"context"
	"net/http"
	"time"
)

// PingServer maintains an HTTP server that responds to get requests
// with a 200 status code.
//
// This is primarily meant as an additional cleanup measure as part
// of the larger liveness process. I.e., other services can use
// ephemeral znodes to determine if the service is responsive.
// Kubernetes (other some other orchestrator that supports liveness
// checks) can use this to determine whether to kill the service.
//
// There may be a small delay where a scheduler begins reassigning
// the unresponsive service's bots but Kubernetes has not yet killed
// the pod. That is fine. More than one bot can run with the same
// token; the messages will simply be processed twice.
type PingServer struct {
	server *http.Server
	listener net.Listener
}

// Serve starts the server in a separate goroutine.
// Port may be given a value of "0" to choose a random port.
func (ps *PingServer) Serve(port string) <-chan error {
	ps.server = &http.Server{
		Handler:      http.HandlerFunc(ps.handler),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	errC := make(chan error)
	go func() {
		listener, err := net.Listen("tcp", ps.getListenAddr(port))
		if err != nil {
			errC <- err
			return
		}
		ps.listener = listener
		
		errC <- ps.server.Serve(listener)
	}()
	return errC
}

func (ps *PingServer) getListenAddr(port string) string {
	if port == "0" {
		return "127.0.0.1:0"
	}
	return ":" + port
}

func (ps *PingServer) handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != ps.Path() {
		w.WriteHeader(http.StatusNotFound)
	} else if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

// Shutdown attempts to gracefully shut down the server.
func (ps *PingServer) Shutdown(timeout time.Duration) error {
	ctx, _ := context.WithTimeout(context.TODO(), timeout)
	return ps.server.Shutdown(ctx)
}

// Path returns the relative path where ping requests are handled.
func (ps *PingServer) Path() string {
	return "/v1/ping"
}

// Port returns the port where the server is accepting requests.
func (ps *PingServer) Port() string {
	addr := ps.listener.Addr().String()
	return strings.Split(addr, ":")[1]
}
