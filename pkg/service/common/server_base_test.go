package common_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/cluster/mocks"
	"gitlab.com/botbin/client/pkg/data"
	"gitlab.com/botbin/client/pkg/service/common"
	"gitlab.com/botbin/client/pkg/service/serial"
	"gitlab.com/botbin/client/pkg/util"
)

// Tests ServerBase
type ServerBaseTestSuite struct {
	suite.Suite
}

func (ss *ServerBaseTestSuite) TestNewClusterServerBase() {
	if util.GetTestMode() != util.TestingAll {
		return
	}

	conn, err := data.GetZK()
	ss.Nil(err)
	api, err := cluster.NewZKClientAPI(conn, "/")
	ss.Nil(err)

	state := new(mocks.State)
	state.On("GetClientAPI").Return(api, nil)
	state.On("GetElectionAPI").Return(new(mocks.ElectionAPI), nil)

	clients, err := api.GetRunningClients()
	ss.Nil(err)
	oldLen := len(clients)

	_, err = common.NewClusterServerBase("80", state)
	ss.Nil(err)

	clients, err = api.GetRunningClients()
	ss.Nil(err)
	ss.Equal(oldLen+1, len(clients))

	state.AssertExpectations(ss.T())
}

func (ss *ServerBaseTestSuite) TestGetLeader() {
	leaderAddr := cluster.Address("127.0.0.1:80")

	client := new(mocks.ClientFacade)
	client.On("GetAddress").Return(leaderAddr)

	electionAPI := new(mocks.ElectionAPI)
	electionAPI.On("GetLeader").Return(client, nil)

	base := common.NewServerBase("8080", electionAPI)
	res, err := base.GetLeader(context.Background(), new(serial.GetLeaderRequest))
	ss.Nil(err)
	ss.Equal(string(leaderAddr), res.GetLeaderAddress())

	client.AssertExpectations(ss.T())
	electionAPI.AssertExpectations(ss.T())
}

func (ss *ServerBaseTestSuite) TestGetState() {
	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	state, err := base.GetState(context.Background(), new(serial.GetStateRequest))
	ss.Nil(err)
	ss.Equal(common.StateUnavailable, int32(state.GetState()))
}

func (ss *ServerBaseTestSuite) TestForceTerminate() {
	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	go base.ForceTerminate(context.Background(), new(serial.TerminateRequest))

	select {
	case <-base.ErrC:
	case <-time.NewTimer(time.Second).C:
		ss.Fail("timed out")
	}
}

func (ss *ServerBaseTestSuite) TestClose() {
	base := common.NewServerBase("8080", new(mocks.ElectionAPI))
	go base.CloseWithError()

	select {
	case <-base.ErrC:
	case <-time.NewTimer(time.Second).C:
		ss.Fail("timed out")
	}
}

func TestServerBase(t *testing.T) {
	suite.Run(t, new(ServerBaseTestSuite))
}
