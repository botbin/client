package common

import (
	"context"
	"errors"
	"net"
	"sync/atomic"

	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/service/serial"
	"gitlab.com/botbin/client/pkg/util"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

// A Server implements base functionality for ServerImpl objects and
// forwards remaining requests to their RPC methods.
type Server interface {
	serial.ClientServer
	Use(ServerImpl) error
	Connect() <-chan error
	Close()
	CloseWithError()
}

// ServerImpl is a hotswappable server that gives a Server its functionality.
type ServerImpl interface {
	// --- These are a subset of the serial.ClientServer interface
	Start(context.Context, *serial.StartRequest) (*serial.StartResponse, error)
	Stop(context.Context, *serial.StopRequest) (*serial.StopResponse, error)
	Update(context.Context, *serial.UpdateRequest) (*serial.UpdateResponse, error)
	Terminate(context.Context, *serial.TerminateRequest) (*serial.TerminateResponse, error)
	GetWeight(context.Context, *serial.GetWeightRequest) (*serial.GetWeightResponse, error)
	// ---

	TearDown()
}

// Server states.
// These correspond to the similar states in the protobuf code, but they are
// directly int32. This allows for easy use with the atomic package.
const (
	// StateUnavailable represents a Server that is not ready to process requests.
	StateAvailable int32 = iota

	// StateAvailable represents a Server that is ready to process requests.
	StateUnavailable

	// StateTerminating represents a Server that is preparing to shut down.
	StateTerminating

	// StateTerminated represents a Server that has shut down.
	StateTerminated
)

// ServerBase implements the Server methods that are common to
// both followers and the leader.
type ServerBase struct {
	conn *grpc.Server
	impl ServerImpl

	electionAPI cluster.ElectionAPI

	// Address indicates where external services can reach the server.
	Address cluster.Address

	// State represents the state of the server.
	// Access should be done through methods in the atomic package.
	State int32

	// ErrC is the channel used to relay any fatal error that
	// occurs during execution. Upon termination, this value will
	// be given nil.
	ErrC chan error
}

var (
	errServerUnavailable = errors.New("the server is currently unable to process this request")
)

// NewClusterServerBase creates a ServerBase that can be embedded in other structs
// to make Server implementations. To start those implementations, pass their
// value to the Connect method along with the port to listen at.
//
// When this method is called, it will register the client with the cluster. This
// is done so that other clients can detect when it terminates. That also means
// this method should only be called once.
func NewClusterServerBase(port string, state cluster.State) (*ServerBase, error) {
	clientAPI, err := state.GetClientAPI()
	if err != nil {
		return nil, nil
	}

	elections, err := state.GetElectionAPI()
	if err != nil {
		return nil, err
	}

	base := NewServerBase(port, elections)
	return base, clientAPI.Register(base.Address)
}

// NewServerBase creates a ServerBase that can be embedded in other structs
// to make Server implementations. To start those implementations, pass their
// value to the Connect method along with the port to listen at.
//
// This method is most likely not what you want. For production use, use the
// NewClusterServerBase method. This is kept separate for testing purposes.
func NewServerBase(port string, elections cluster.ElectionAPI) *ServerBase {
	return &ServerBase{
		conn:        grpc.NewServer(),
		electionAPI: elections,
		Address:     cluster.Address(util.GetIP() + ":" + port),
		State:       StateUnavailable,
		ErrC:        make(chan error),
	}
}

// Use sets the server implementation.
//
// This method will put the server in an unavailable state during the switch.
// During that time, some requests will fail. This is needed so that requests
// meant for one type of server implementation don't make their way to another
// implementation and harm the entire service.
func (sb *ServerBase) Use(impl ServerImpl) error {
	old := atomic.SwapInt32(&sb.State, StateUnavailable)
	if old == StateTerminated || old == StateTerminating {
		return errors.New("can't supply server impl during terminated or terminating states")
	}

	if sb.impl != nil {
		sb.impl.TearDown()
	}
	sb.impl = impl
	atomic.StoreInt32(&sb.State, StateAvailable)
	return nil
}

// GetLeader returns the address of the leader of the cluster.
//
// All other outside requests to the client cluster must be sent to
// the leader. Thus, external services will need to invoke this
// method upon first interaction and if the leader dies.
func (sb *ServerBase) GetLeader(ctx context.Context, req *serial.GetLeaderRequest) (*serial.GetLeaderResponse, error) {
	leader, err := sb.electionAPI.GetLeader()
	if err != nil {
		return nil, err
	}
	
	return &serial.GetLeaderResponse{
		LeaderAddress: string(leader.GetAddress()),
	}, nil
}

// Start executes the Start RPC on the server implementation.
func (sb *ServerBase) Start(ctx context.Context, req *serial.StartRequest) (*serial.StartResponse, error) {
	if sb.isUnavailable() {
		return nil, errServerUnavailable
	}
	return sb.impl.Start(ctx, req)
}

// Stop executes the Stop RPC on the server implementation.
func (sb *ServerBase) Stop(ctx context.Context, req *serial.StopRequest) (*serial.StopResponse, error) {
	if sb.isUnavailable() {
		return nil, errServerUnavailable
	}
	return sb.impl.Stop(ctx, req)
}

// Update executes the Update RPC on the server implementation.
func (sb *ServerBase) Update(ctx context.Context, req *serial.UpdateRequest) (*serial.UpdateResponse, error) {
	if sb.isUnavailable() {
		return nil, errServerUnavailable
	}
	return sb.impl.Update(ctx, req)
}

// Terminate executes the Terminate RPC on the server implementation.
func (sb *ServerBase) Terminate(ctx context.Context, req *serial.TerminateRequest) (*serial.TerminateResponse, error) {
	if sb.isUnavailable() {
		return nil, errServerUnavailable
	}
	atomic.StoreInt32(&sb.State, StateTerminating)
	return sb.impl.Terminate(ctx, req)
}

// GetWeight executes the GetWeight RPC on the server implementation.
func (sb *ServerBase) GetWeight(ctx context.Context, req *serial.GetWeightRequest) (*serial.GetWeightResponse, error) {
	if sb.isUnavailable() {
		return nil, errServerUnavailable
	}
	return sb.impl.GetWeight(ctx, req)
}

func (sb *ServerBase) isUnavailable() bool {
	return atomic.LoadInt32(&sb.State) == StateUnavailable
}

// ForceTerminate forcibly shuts down the client without waiting for its
// bots to be stopped.
func (sb *ServerBase) ForceTerminate(ctx context.Context,
	req *serial.TerminateRequest) (*serial.TerminateResponse, error) {
	atomic.StoreInt32(&sb.State, StateTerminating)
	zap.S().Infow("received force termination signal", "reason", req.GetReason())
	defer sb.CloseWithError()
	return new(serial.TerminateResponse), nil
}

// GetState returns the state of the client.
func (sb *ServerBase) GetState(ctx context.Context,
	req *serial.GetStateRequest) (*serial.GetStateResponse, error) {
	return &serial.GetStateResponse{
		State: sb.getState(),
	}, nil
}

func (sb *ServerBase) getState() serial.ClientState {
	switch atomic.LoadInt32(&sb.State) {
	case StateTerminating:
		return serial.ClientState_Terminating

	case StateAvailable:
		return serial.ClientState_Available

	default:
		return serial.ClientState_Unavailable
	}
}

// Connect opens the server up to requests.
//
// This method will not block. Any error is passed over the returned channel.
func (sb *ServerBase) Connect() <-chan error {
	go func() {
		serial.RegisterClientServer(sb.conn, sb)

		listener, err := net.Listen("tcp", ":"+sb.Address.Port())
		if err != nil {
			sb.ErrC <- err
			return
		}

		atomic.StoreInt32(&sb.State, StateAvailable)
		sb.ErrC <- sb.conn.Serve(listener)
	}()
	return sb.ErrC
}

// Close stops the server gracefully.
//
// This will not send an error to the error channel.
func (sb *ServerBase) Close() {
	sb.closeAndDo(func() {})
}

// CloseWithError stops the server gracefully.
//
// This will send a nil error to the error channel.
func (sb *ServerBase) CloseWithError() {
	sb.closeAndDo(func() { sb.ErrC <- nil })
}

func (sb *ServerBase) closeAndDo(do func()) {
	oldState := atomic.SwapInt32(&sb.State, StateTerminated)
	if oldState != StateTerminated {
		do()

		sb.conn.GracefulStop()
		if sb.impl != nil {
			sb.impl.TearDown()
		}
	}
}
