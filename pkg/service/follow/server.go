package follow

import (
	"context"
	"errors"
	"sync/atomic"
	"time"

	"gitlab.com/botbin/client/pkg/bot"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/metrics"
	"gitlab.com/botbin/client/pkg/service/common"
	"gitlab.com/botbin/client/pkg/service/serial"
	"go.uber.org/zap"
)

// Server implements the common.ServerImpl interface for followers.
type Server struct {
	base   *common.ServerBase
	botAPI cluster.BotAPI

	bots      *roster
	maxWeight float64
	reporter  metrics.Reporter
}

// New creates a common.Server that performs follower logic.
func New(base *common.ServerBase, state cluster.State, maxWeight float64) (common.ServerImpl, error) {
	botAPI, err := state.GetBotAPI()
	if err != nil {
		return nil, err
	}

	s := &Server{
		base:      base,
		botAPI:    botAPI,
		bots:      newRoster(),
		maxWeight: maxWeight,
		reporter:  metrics.NewReporter(10*time.Second, botAPI, base.Address),
	}
	return s, base.Use(s)
}

// TODO: The server should only execute Start logic if the request came from the leader.
//
// It would probably be best to forward the start request to the leader by means of a
// cluster.ClientFacade. We can then put the leader address in the response so the sender
// knows where to send future requests.

// Start creates a bot instance for the client to manage.
func (s *Server) Start(ctx context.Context, req *serial.StartRequest) (*serial.StartResponse, error) {
	if !s.hasRoom(req.GetWeight()) {
		return nil, errors.New("bot weight exceeds current free resources")
	}

	bot, err := s.setUpBot(req)
	if err != nil {
		return nil, err
	}

	s.reporter.Use(req.GetId(), bot.Meter())
	s.reporter.Report()
	return new(serial.StartResponse), nil
}

func (s *Server) hasRoom(requestedWeight float64) bool {
	lastWeight := s.reporter.LastServiceWeight()
	return requestedWeight+lastWeight <= s.maxWeight
}

func (s *Server) setUpBot(req *serial.StartRequest) (bot.Bot, error) {
	bot, err := bot.New(req.GetId(), req.GetToken(), req.GetWeight())
	if err != nil {
		zap.S().Infow("failed to create bot client", "bot.id", req.GetId(), "error", err)
		return nil, err
	}

	if err := s.bots.Add(req, bot); err != nil {
		bot.Disconnect()
		zap.S().Infow("failed to add bot to roster", "bot.id", req.GetId(), "error", err)
		return nil, err
	}
	return bot, s.registerWithCluster(req)
}

func (s *Server) registerWithCluster(req *serial.StartRequest) error {
	if err := s.botAPI.SetLocation(req.GetId(), s.base.Address); err != nil {
		s.bots.Remove(req.GetId())
		zap.S().Errorw("failed to register bot location", "bot.id", req.GetId(), "error", err)
		return err
	}

	cbot := &cluster.Bot{
		ID: req.GetId(),
		Weight: req.GetWeight(),
	}
	return s.botAPI.Save(cbot, s.base.Address)
}

// Stop tears down a bot instance.
func (s *Server) Stop(ctx context.Context, req *serial.StopRequest) (*serial.StopResponse, error) {
	// TODO: Ensure failure in Stop doesn't leave an inconsistent state.

	s.reporter.Remove(req.GetId())
	_, err := s.bots.Remove(req.GetId())
	if err != nil {
		return nil, err
	}

	if err := s.botAPI.RemoveLocation(req.GetId()); err != nil {
		zap.S().Infow("failed to remove bot from location registry", "bot.id", req.GetId(), "error", err)
		return nil, err
	}

	s.reporter.Report()
	defer s.tryCompleteTermination()
	return new(serial.StopResponse), nil
}

// Update replaces the configuration of a bot without restarting it.
func (s *Server) Update(ctx context.Context, req *serial.UpdateRequest) (*serial.UpdateResponse, error) {
	err := s.bots.Replace(req)
	return new(serial.UpdateResponse), err
}

// Terminate signals to the client that it should shut down once all of
// its bots have been stopped.
//
// This will put the client in a Terminating state. While terminating,
// any request to Start a bot will fail.
func (s *Server) Terminate(ctx context.Context, req *serial.TerminateRequest) (*serial.TerminateResponse, error) {
	zap.S().Infow("received termination signal", "reason", req.GetReason())
	defer s.tryCompleteTermination()
	return new(serial.TerminateResponse), nil
}

func (s *Server) tryCompleteTermination() {
	if atomic.LoadInt32(&s.base.State) == common.StateTerminating && s.bots.Size() == 0 {
		zap.S().Info("last bot removed; completing termination process...")
		s.base.CloseWithError()
	}
}

// GetWeight returns the current weight of the client.
//
// The weight of a client is the sum of the weights of its bots.
func (s *Server) GetWeight(context.Context, *serial.GetWeightRequest) (*serial.GetWeightResponse, error) {
	return &serial.GetWeightResponse{
		Weight: s.reporter.LastServiceWeight(),
	}, nil
}

// TearDown gracefully releases acquired resources.
func (s *Server) TearDown() {
	s.reporter.Report()
	s.reporter.Stop()
	s.bots.Clear()
	// We won't call s.botAPI.MarkInactive because that is the job of the leader.
}
