package follow

import (
	"encoding/base64"
	"errors"
	"fmt"
	"sync"

	"gitlab.com/botbin/client/pkg/service/serial"

	"github.com/golang/protobuf/proto"
	"gitlab.com/botbin/client/pkg/bot"
	"go.uber.org/zap"
	"sync/atomic"
)

// roster maintains the service collection of bots.
type roster struct {
	mut  sync.RWMutex
	m    map[int64]*rosterEntry
	size int32

	applier *bot.ConfigApplier
}

type rosterEntry struct {
	// Request defines how to create the bot as it is now.
	Request *serial.StartRequest
	// Bot is the running bot instance.
	Bot bot.Bot
}

var (
	errRosterSlotFilled = errors.New("cannot satisfy request; bot id already exists")
	errRosterSlotEmpty  = errors.New("no bot exists with the given id")
)

// newRoster configures a roster instance.
func newRoster() *roster {
	return &roster{
		m:       make(map[int64]*rosterEntry),
		applier: bot.NewApplier(),
	}
}

// Size returns the number of items in the roster.
func (r *roster) Size() int32 {
	return atomic.LoadInt32(&r.size)
}

// Get retrieves the entry that matches the bot ID.
func (r *roster) Get(botID int64) (*rosterEntry, error) {
	r.mut.RLock()
	defer r.mut.RUnlock()

	entry, exists := r.m[botID]
	if !exists {
		return nil, errRosterSlotEmpty
	}
	return entry, nil
}

// Add inserts a bot into the roster if there is not already one with
// with the same bot ID. If the bot already has a config, it will
// be replaced with the setup from the request.
func (r *roster) Add(req *serial.StartRequest, bot bot.Bot) error {
	r.mut.Lock()
	defer r.mut.Unlock()

	if req.GetId() != bot.ID() {
		return fmt.Errorf("request id %d and bot id %d must match", req.GetId(), bot.ID())
	}

	if _, exists := r.m[req.GetId()]; exists {
		return errRosterSlotFilled
	}

	if err := bot.Connect(); err != nil {
		return err
	}

	if err := r.applyConfig(bot, req); err != nil {
		return err
	}

	r.m[req.GetId()] = &rosterEntry{
		Bot:  bot,
		Request: req,
	}
	atomic.AddInt32(&r.size, 1)
	return nil
}

// getConfig retrieves config content from a request that contains it.
func (r *roster) getConfig(m proto.Message) ([]byte, error) {
	switch request := m.(type) {
	case *serial.StartRequest:
		return r.extract(request.GetConfig())
	case *serial.UpdateRequest:
		return r.extract(request.GetConfig())
	default:
		return nil, errors.New("can only get config from *StartRequest or *UpdateRequest")
	}
}

// extract retrieves the content from a config in the proper format.
func (r *roster) extract(config *serial.Config) ([]byte, error) {
	if config.GetB64Encoded() {
		return base64.StdEncoding.DecodeString(config.GetContent())
	}
	return []byte(config.GetContent()), nil
}

// Replace replaces the previous request that matches this requests
// bot ID or returns an error if none existed.
func (r *roster) Replace(req *serial.UpdateRequest) error {
	r.mut.Lock()
	defer r.mut.Unlock()

	old, exists := r.m[req.GetId()]
	if !exists {
		return errRosterSlotEmpty
	}

	if err := r.applyConfig(old.Bot, req); err != nil {
		return err
	}

	old.Request.Config = req.GetConfig()
	return nil
}

// applyConfig gets the config from a request and applies it to the bot
// using the roster's applier.
// req should be either a *StartRequest or *UpdateRequest.
func (r *roster) applyConfig(bot bot.Bot, req proto.Message) error {
	config, err := r.getConfig(req)
	if err != nil {
		return err
	}
	return r.applier.ApplyYAML(bot, config)
}

// Remove deletes the entry with a matching bot ID and returns that
// entry if successful. The bot will be disconnected during removal.
func (r *roster) Remove(botID int64) (*serial.StartRequest, error) {
	r.mut.Lock()
	defer r.mut.Unlock()

	entry, exists := r.m[botID]
	if !exists {
		return nil, errRosterSlotEmpty
	}
	return entry.Request, r.remove(entry)
}

// Clear removes all entries from the roster.
// All clients will be disconnected during removal.
func (r *roster) Clear() {
	r.mut.Lock()
	defer r.mut.Unlock()

	for botID, entry := range r.m {
		err := r.remove(entry)
		if err != nil {
			zap.S().Errorw("error removing entry", "error", err, "bot.id", botID)
		}
	}
}

func (r *roster) remove(entry *rosterEntry) error {
	err := entry.Bot.Disconnect()
	delete(r.m, entry.Request.GetId())
	atomic.AddInt32(&r.size, -1)
	return err
}
