package follow

import (
	"testing"

	"gopkg.in/yaml.v2"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/bot/mocks"
	"gitlab.com/botbin/client/pkg/service/serial"
	"gitlab.com/botbin/botconf"
)

// Tests roster
type RosterTestSuite struct {
	suite.Suite
}

func (rs *RosterTestSuite) makeConfig(name string) string {
	config := &botconf.BotConfig{
		Version: "1",
		Metadata: botconf.BotMetadata{
			Name:    name,
			Version: "0.1.0",
		},
		Spec: botconf.BotSpec{
			Trigger: ".",
			Modules: []botconf.Module{
				{
					Name:   "ping",
					Tag:    "1.2",
					Author: "tester",
					Scopes: []string{"test"},
				},
			},
		},
	}
	out, err := yaml.Marshal(config)
	rs.Nil(err)
	return string(out)
}

// Ensures that the various methods can be used together as one
// would expect.
func (rs *RosterTestSuite) TestAcceptance() {
	start := &serial.StartRequest{
		Id:    1,
		Token: "testToken",
		Config: &serial.Config{
			Content: rs.makeConfig("first"),
		},
	}
	update := &serial.UpdateRequest{
		Id: start.Id,
		Config: &serial.Config{
			Content: rs.makeConfig("second"),
		},
	}
	bot := rs.getMockClient(start.Id)

	bots := newRoster()
	rs.Nil(bots.Add(start, bot))

	entry, err := bots.Get(start.Id)
	rs.Nil(err)
	rs.EqualValues(start, entry.Request)

	rs.Nil(bots.Replace(update))

	req, err := bots.Remove(start.Id)
	rs.Nil(err)
	rs.Equal(start.Id, req.Id)
	rs.Equal(start.Token, req.Token)
	rs.Equal(update.Config, req.Config)

	bot.AssertExpectations(rs.T())
}

func (rs *RosterTestSuite) TestReplace_Missing() {
	update := &serial.UpdateRequest{
		Id: 1,
		Config: &serial.Config{
			Content: rs.makeConfig("test"),
		},
	}

	requests := newRoster()
	rs.Equal(errRosterSlotEmpty, requests.Replace(update))
}

func (rs *RosterTestSuite) TestRemove_Missing() {
	requests := newRoster()
	req, err := requests.Remove(1)
	rs.Nil(req)
	rs.Equal(errRosterSlotEmpty, err)
}

func (rs *RosterTestSuite) TestAdd_Existing() {
	start := &serial.StartRequest{
		Id:    1,
		Token: "testToken",
		Config: &serial.Config{
			Content: rs.makeConfig("test"),
		},
	}

	bot := new(mocks.Client)
	bot.On("Connect").Return(nil)
	bot.On("Apply", mock.Anything).Return(nil)
	bot.On("ID").Return(start.Id)

	requests := newRoster()
	rs.Nil(requests.Add(start, bot))
	rs.Equal(errRosterSlotFilled, requests.Add(start, bot))

	bot.AssertExpectations(rs.T())
}

func (rs *RosterTestSuite) TestClear() {
	bots := newRoster()

	a := &serial.StartRequest{
		Id:    1,
		Token: "testToken",
		Config: &serial.Config{
			Content: rs.makeConfig("test"),
		},
	}
	botA := rs.getMockClient(a.Id)
	rs.Nil(bots.Add(a, botA))

	b := &serial.StartRequest{
		Id:    2,
		Token: "testToken",
		Config: &serial.Config{
			Content: rs.makeConfig("test"),
		},
	}
	botB := rs.getMockClient(b.Id)
	rs.Nil(bots.Add(b, botB))

	bots.Clear()
	_, err := bots.Get(a.Id)
	rs.NotNil(err)
	_, err = bots.Get(b.Id)
	rs.NotNil(err)

	botA.AssertExpectations(rs.T())
	botB.AssertExpectations(rs.T())
}

func (rs *RosterTestSuite) getMockClient(id int64) *mocks.Client {
	bot := new(mocks.Client)
	bot.On("Connect").Return(nil)
	bot.On("Apply", mock.Anything).Return(nil)
	bot.On("Disconnect").Return(nil)
	bot.On("ID").Return(id)
	return bot
}

func (rs *RosterTestSuite) TestGet_Missing() {
	requests := newRoster()
	req, err := requests.Get(1)
	rs.Nil(req)
	rs.Equal(errRosterSlotEmpty, err)
}

func TestRoster(t *testing.T) {
	suite.Run(t, new(RosterTestSuite))
}
