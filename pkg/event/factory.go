package event

import "gitlab.com/botbin/client/pkg/event/serial"

// Factory turns native Discord events into Event types that
// can be quickly and efficiently shared among external systems.
type Factory interface {
	Create(discordEvent interface{}) (*serial.Event, error)
}
