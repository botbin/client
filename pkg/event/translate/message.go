package translate

import (
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/botbin/client/pkg/event/conv"
	"gitlab.com/botbin/client/pkg/event/serial"
)

// DGMessageTranslator translates MessageCreate events into protobuf events.
type DGMessageTranslator struct {
}

// Translate turns a discordgo.MessageCreate object into a *serial.Event.
func (dt DGMessageTranslator) Translate(discordEvent interface{}) (*serial.Event, error) {
	dgoEvent, ok := discordEvent.(*discordgo.MessageCreate)
	if !ok {
		return nil, dt.unrecognizedEventError(discordEvent)
	}

	event := &serial.Event{Timestamp: int32(dt.getUnixTimestamp(dgoEvent))}
	dt.storeState(dgoEvent, event)
	return event, nil
}

func (dt DGMessageTranslator) unrecognizedEventError(discordEvent interface{}) error {
	return errors.New(fmt.Sprint("expected MessageCreate event; got %T", discordEvent))
}

func (dt DGMessageTranslator) getUnixTimestamp(event *discordgo.MessageCreate) int64 {
	timestamp, err := event.Timestamp.Parse()
	if err != nil {
		// Looks good to me.
		return time.Now().Unix()
	}

	return timestamp.Unix()
}

func (dt DGMessageTranslator) storeState(src *discordgo.MessageCreate, dst *serial.Event) {
	// TODO: Handle guild messages.
	// If the message was sent in a guild, we should be able to fetch
	// the guild id and name using the channel id. Then we'll need to
	// make dst.State a *serial.Event_GuildMessage type with the added
	// information.

	dst.State = &serial.Event_DirectMessage{
		DirectMessage: dt.getMessage(src),
	}
}

func (dt DGMessageTranslator) getMessage(event *discordgo.MessageCreate) *serial.Message {
	return &serial.Message{
		Id:      conv.FromSnowflake(event.ID),
		Author:  dt.getAuthor(event),
		Channel: dt.getChannel(event),
		Content: event.Content,
	}
}

func (dt DGMessageTranslator) getAuthor(event *discordgo.MessageCreate) *serial.Author {
	return &serial.Author{
		Identity: &serial.NamedEntity{
			Id:   conv.FromSnowflake(event.Author.ID),
			Name: event.Author.Username,
		},
	}
}

func (dt DGMessageTranslator) getChannel(event *discordgo.MessageCreate) *serial.NamedEntity {
	return &serial.NamedEntity{
		Id: conv.FromSnowflake(event.ChannelID),
		// TODO: Get the channel name.
	}
}
