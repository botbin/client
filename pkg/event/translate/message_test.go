package translate_test

import (
	"strconv"
	"testing"
	"time"

	"github.com/bwmarrin/discordgo"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/event/serial"
	"gitlab.com/botbin/client/pkg/event/translate"
)

type DGMessageTranslatorTestSuite struct {
	suite.Suite
	translator translate.Translator
}

func (ds *DGMessageTranslatorTestSuite) SetupTest() {
	ds.translator = translate.DGMessageTranslator{}
}

func (ds *DGMessageTranslatorTestSuite) TestTranslate_DirectMessage() {
	id, channelID, authorID, content, ts := 3, 4, 5, "test", time.Now()
	msg := &discordgo.MessageCreate{
		Message: &discordgo.Message{
			ID:        strconv.Itoa(id),
			ChannelID: strconv.Itoa(channelID),
			Content:   content,
			Author:    &discordgo.User{ID: strconv.Itoa(authorID)},
			Timestamp: discordgo.Timestamp(ts.UTC().Format(time.RFC3339)),
		},
	}

	event, err := ds.translator.Translate(msg)
	assert.Nil(ds.T(), err)
	state, ok := event.State.(*serial.Event_DirectMessage)
	assert.True(ds.T(), ok)

	assert.Equal(ds.T(), state.DirectMessage.Id, uint64(id))
	assert.Equal(ds.T(), state.DirectMessage.Author.Identity.Id, uint64(authorID))
	assert.Equal(ds.T(), state.DirectMessage.Channel.Id, uint64(channelID))
	assert.Equal(ds.T(), state.DirectMessage.Content, content)
	assert.Equal(ds.T(), ts.Unix(), int64(event.Timestamp))
}

func TestDGMessageTranslator(t *testing.T) {
	suite.Run(t, new(DGMessageTranslatorTestSuite))
}
