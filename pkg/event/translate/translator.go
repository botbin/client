package translate

import "gitlab.com/botbin/client/pkg/event/serial"

// Translator types convert specific Discord events into protobuf events.
type Translator interface {
	Translate(discordEvent interface{}) (*serial.Event, error)
}
