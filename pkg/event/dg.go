package event

import (
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/botbin/client/pkg/event/serial"
	"gitlab.com/botbin/client/pkg/event/translate"
)

// DGFactory creates protobuf events from objects created by
// the DiscordGo package.
type DGFactory struct {
	Message translate.Translator
}

// Create accepts an event object from DiscordGo and transforms
// it into a serializable protocol buffer.
func (df DGFactory) Create(discordEvent interface{}) (*serial.Event, error) {
	switch e := discordEvent.(type) {
	case *discordgo.MessageCreate:
		return df.Message.Translate(e)

	default:
		return nil, df.unrecognizedEventError(discordEvent)
	}
}

func (df DGFactory) unrecognizedEventError(discordEvent interface{}) error {
	return errors.New(fmt.Sprintf("unrecognized event type %T", discordEvent))
}

// NewDGFactory returns a factory that handles DiscordGo events.
func NewDGFactory() Factory {
	return DGFactory{
		Message: translate.DGMessageTranslator{},
	}
}
