package event_test

import (
	"testing"

	"github.com/bwmarrin/discordgo"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/event"
	"gitlab.com/botbin/client/pkg/event/serial"
)

type DGFactoryTestSuite struct {
	suite.Suite
	factory event.Factory
}

func (ds *DGFactoryTestSuite) SetupTest() {
	ds.factory = event.DGFactory{
		Message: mockDGMessageTranslator{},
	}
}

type mockDGMessageTranslator struct {
}

func (mt mockDGMessageTranslator) Translate(e interface{}) (*serial.Event, error) {
	return &serial.Event{}, nil
}

// TestCreate ensures that translators exist for expected event types.
// This does not validate whether the translators work properly.
func (ds *DGFactoryTestSuite) TestCreate() {
	tests := []createTestCase{
		{
			DGEvent:      &discordgo.MessageCreate{},
			CanMakeEvent: true,
		},
		{
			DGEvent:      "hallo",
			CanMakeEvent: false,
		},
	}

	ds.runCreateTestCases(tests)
}

func (ds *DGFactoryTestSuite) runCreateTestCases(tests []createTestCase) {
	for _, test := range tests {
		event, err := ds.factory.Create(test.DGEvent)

		if test.CanMakeEvent {
			assert.Nil(ds.T(), err)
			assert.NotNil(ds.T(), event)
		} else {
			assert.NotNil(ds.T(), err)
		}
	}
}

type createTestCase struct {
	DGEvent      interface{}
	CanMakeEvent bool
}

func TestDGFactory(t *testing.T) {
	suite.Run(t, new(DGFactoryTestSuite))
}
