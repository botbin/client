package conv

import (
	"strconv"
)

// FromSnowflake converts a snowflake that is in string format
// to its expected type.
func FromSnowflake(snowflake string) uint64 {
	i, _ := strconv.ParseUint(snowflake, 10, 64)
	return i
}
