package conv_test

import (
	"math/rand"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/botbin/client/pkg/event/conv"
)

func TestFromSnowFlake(t *testing.T) {
	i := rand.Uint64()
	istr := strconv.FormatUint(i, 10)

	iAmHopefullyTheSame := conv.FromSnowflake(istr)
	assert.Equal(t, i, iAmHopefullyTheSame)
}
