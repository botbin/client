package bot_test

import (
	"testing"

	"gitlab.com/botbin/botconf"
	"gitlab.com/botbin/client/pkg/bot"
	"gitlab.com/botbin/client/pkg/bot/mocks"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	yaml "gopkg.in/yaml.v2"
)

// Tests ConfigApplier
type ConfigApplierTestSuite struct {
	suite.Suite
}

// Ensures that ApplyYAML supplies the Applyee with the deserialized
// config if it was valid.
func (cs *ConfigApplierTestSuite) TestApplyYAML_Valid() {
	config := &botconf.BotConfig{
		Version: "1",
		Metadata: botconf.BotMetadata{
			Name:    "test",
			Version: "0.1.0",
		},
		Spec: botconf.BotSpec{
			Trigger: ".",
			Modules: []botconf.Module{
				{
					Name:   "ping",
					Tag:    "1.2",
					Author: "tester",
					Scopes: []string{"test"},
				},
			},
		},
	}
	serialized, err := yaml.Marshal(config)
	cs.Nil(err)

	applyee := new(mocks.Applyee)
	applyee.On("Apply", mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		actual := args[0].(*botconf.BotConfig)
		cs.EqualValues(config, actual)
	})

	err = bot.NewApplier().ApplyYAML(applyee, serialized)
	cs.Nil(err)
	applyee.AssertExpectations(cs.T())
}

// Ensure that invalid YAML configs are not applied to the Applyee.
func (cs *ConfigApplierTestSuite) TestApplyYAML_Invalid() {
	config := &botconf.BotConfig{Version: "1"}
	serialized, err := yaml.Marshal(config)
	cs.Nil(err)

	applyee := new(mocks.Applyee)

	err = bot.NewApplier().ApplyYAML(applyee, serialized)
	cs.NotNil(err)
}

func TestConfigApplier(t *testing.T) {
	suite.Run(t, new(ConfigApplierTestSuite))
}
