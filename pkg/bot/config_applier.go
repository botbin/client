package bot

import (
	"gitlab.com/botbin/botconf"
)

// An Applyee applies a bot config to itself.
type Applyee interface {
	Apply(*botconf.BotConfig) error
}

// ConfigApplier applies raw bot configurations to Applyees.
type ConfigApplier struct {
	validator botconf.Validator
	loader botconf.ConfigLoader
}

// NewApplier configures a ConfigApplier instance.
func NewApplier() *ConfigApplier {
	return &ConfigApplier{
		validator: botconf.NewV1Validator(),
		loader: botconf.NewYAMLLoader(),
	}
}

// ApplyYAML applies a YAML bot configuration to an Applyee.
func (ca *ConfigApplier) ApplyYAML(ap Applyee, rawConfig []byte) error {
	config, err := ca.loader.Load(rawConfig)
	if err != nil {
		return err
	}

	if errs := ca.validator.Validate(config); len(errs) != 0 {
		return errs[0]
	}
	return ap.Apply(config)
}
