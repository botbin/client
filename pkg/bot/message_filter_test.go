package bot_test

import (
	"testing"

	"github.com/bwmarrin/discordgo"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/bot"
)

type NewMessageFilterTestSuite struct {
	suite.Suite
}

// The filter should accept all events that aren't MessageCreate.
func (ns *NewMessageFilterTestSuite) TestNonMessage() {
	called := false
	filter := bot.NewMessageFilter(".", func(e interface{}) {
		called = true
	})
	filter(nil, &discordgo.GuildBan{})

	assert.True(ns.T(), called)
}

// The filter should ignore messages without the trigger.
func (ns *NewMessageFilterTestSuite) TestIgnored() {
	called := false
	filter := bot.NewMessageFilter(".", func(e interface{}) {
		called = true
	})
	filter(nil, &discordgo.MessageCreate{
		Message: &discordgo.Message{
			Content: "test",
		},
	})

	assert.False(ns.T(), called)
}

// The filter should accept messages with the trigger and remove the
// trigger from the message.
func (ns *NewMessageFilterTestSuite) TestAccepted() {
	called := false
	const trigger = "."
	content := trigger + "test"

	filter := bot.NewMessageFilter(trigger, func(e interface{}) {
		called = true
		msg := e.(*discordgo.MessageCreate)
		assert.Equal(ns.T(), content[len(trigger):], msg.Message.Content)
	})

	filter(nil, &discordgo.MessageCreate{
		Message: &discordgo.Message{
			Content: content,
		},
	})

	assert.True(ns.T(), called)
}

func TestNewMessageFilter(t *testing.T) {
	suite.Run(t, new(NewMessageFilterTestSuite))
}
