package bot

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

// NewMessageFilter returns an event handler for DiscordGo that only calls handler
// if the event type is not *discordgo.MessageCreate or if it is but starts with
// the provided string. Filtered messages will have their starting string removed
// before being passed on to handler.
func NewMessageFilter(startsWith string, handler func(event interface{})) func(s *discordgo.Session, event interface{}) {
	return func(s *discordgo.Session, event interface{}) {
		if msgCreate, ok := event.(*discordgo.MessageCreate); ok {
			if strings.Index(msgCreate.Content, startsWith) == 0 {
				msgCreate.Content = msgCreate.Content[1:]
				handler(event)
			}
		} else {
			handler(event)
		}
	}
}
