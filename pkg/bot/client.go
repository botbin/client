package bot

import (
	"sync"

	"github.com/bwmarrin/discordgo"
	gom "github.com/rcrowley/go-metrics"
	"gitlab.com/botbin/client/pkg/event"
	conf "gitlab.com/botbin/botconf"
	"go.uber.org/zap"
)

// A Bot processes the events of a Discord bot.
type Bot interface {
	Connect() error
	Disconnect() error
	Apply(config *conf.BotConfig) error
	ID() int64
	Meter() gom.Meter
}

// botClient implements the Bot interface.
type botClient struct {
	id     int64
	config *conf.BotConfig

	session         *discordgo.Session
	openConnection  sync.Once
	closeConnection sync.Once

	eventFactory        event.Factory
	removeMessageFilter func()

	meter gom.Meter
}

// New creates bot client.
//
// The returned client is not started but can be put in that
// state by calling Connect().
func New(id int64, token string, weight float64) (Bot, error) {
	session, err := discordgo.New("Bot " + token)
	if err != nil {
		return nil, err
	}

	meter := gom.NewMeter()
	meter.Mark(int64(weight))

	return &botClient{
		id:           id,
		session:      session,
		eventFactory: event.NewDGFactory(),
		meter:        meter,
	}, nil
}

// Connect opens a connection to the Discord gateway.
func (bc *botClient) Connect() error {
	var err error
	bc.openConnection.Do(func() {
		bc.closeConnection = sync.Once{}
		err = bc.session.Open()
	})
	return err
}

// Disconnect closes the client's connection to Discord.
func (bc *botClient) Disconnect() error {
	var err error
	bc.closeConnection.Do(func() {
		bc.meter.Stop()
		bc.openConnection = sync.Once{}
		err = bc.session.Close()
	})
	return err
}

// Apply updates the client with a new configuration.
func (bc *botClient) Apply(config *conf.BotConfig) error {
	bc.config = config
	zap.S().Infow("applied bot config",
		"bot.id", bc.id,
		"bot.config", config,
	)

	if bc.removeMessageFilter != nil {
		bc.removeMessageFilter()
	}
	bc.removeMessageFilter = bc.session.AddHandler(NewMessageFilter(config.Spec.Trigger, bc.handleEvent))
	return nil
}

func (bc *botClient) handleEvent(genericEvent interface{}) {
	bc.meter.Mark(1)
	serialized, err := bc.eventFactory.Create(genericEvent)
	if err != nil {
		return
	}

	// TODO: Send event to module instead of logging.
	// And even then, we need to be careful about what is logged.
	// Event type? Okay. Private message contents? Not okay.
	zap.S().Infow("serialized event",
		"event", serialized,
	)
}

// ID returns the unique ID of the bot.
func (bc *botClient) ID() int64 {
	return bc.id
}

// Meter returns the meter used for recording events.
func (bc *botClient) Meter() gom.Meter {
	return bc.meter
}
