// Code generated by mockery v1.0.0. DO NOT EDIT.
package mocks

import metrics "github.com/rcrowley/go-metrics"
import mock "github.com/stretchr/testify/mock"

// Meter is an autogenerated mock type for the Meter type
type Meter struct {
	mock.Mock
}

// Count provides a mock function with given fields:
func (_m *Meter) Count() int64 {
	ret := _m.Called()

	var r0 int64
	if rf, ok := ret.Get(0).(func() int64); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(int64)
	}

	return r0
}

// Mark provides a mock function with given fields: _a0
func (_m *Meter) Mark(_a0 int64) {
	_m.Called(_a0)
}

// Rate1 provides a mock function with given fields:
func (_m *Meter) Rate1() float64 {
	ret := _m.Called()

	var r0 float64
	if rf, ok := ret.Get(0).(func() float64); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(float64)
	}

	return r0
}

// Rate15 provides a mock function with given fields:
func (_m *Meter) Rate15() float64 {
	ret := _m.Called()

	var r0 float64
	if rf, ok := ret.Get(0).(func() float64); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(float64)
	}

	return r0
}

// Rate5 provides a mock function with given fields:
func (_m *Meter) Rate5() float64 {
	ret := _m.Called()

	var r0 float64
	if rf, ok := ret.Get(0).(func() float64); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(float64)
	}

	return r0
}

// RateMean provides a mock function with given fields:
func (_m *Meter) RateMean() float64 {
	ret := _m.Called()

	var r0 float64
	if rf, ok := ret.Get(0).(func() float64); ok {
		r0 = rf()
	} else {
		r0 = ret.Get(0).(float64)
	}

	return r0
}

// Snapshot provides a mock function with given fields:
func (_m *Meter) Snapshot() metrics.Meter {
	ret := _m.Called()

	var r0 metrics.Meter
	if rf, ok := ret.Get(0).(func() metrics.Meter); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(metrics.Meter)
		}
	}

	return r0
}

// Stop provides a mock function with given fields:
func (_m *Meter) Stop() {
	_m.Called()
}
