package metrics

import (
	"sync"
	"time"

	gom "github.com/rcrowley/go-metrics"
	"gitlab.com/botbin/client/pkg/cluster"
	"go.uber.org/zap"
)

// A Reporter reports the state of metrics.
type Reporter interface {
	Start(interval time.Duration)
	Stop()
	Report()
	LastServiceWeight() float64
	Use(key int64, meter gom.Meter)
	Remove(key int64)
}

// serviceReporter implements the Reporter interface.
//
// It is meant to report the metrics of the entire application by
// means of individual bot tallies and a total service aggregate.
type serviceReporter struct {
	meterLock     sync.Mutex
	metersByBotID map[int64]gom.Meter

	botAPI            cluster.BotAPI
	lastServiceWeight float64
	self              cluster.Address

	stopSignal chan bool
}

// NewReporter configures and starts a Reporter that sends bot weights to
// the shared cluster state.
func NewReporter(interval time.Duration, botAPI cluster.BotAPI,
	self cluster.Address) Reporter {

	reporter := &serviceReporter{
		metersByBotID: make(map[int64]gom.Meter),
		botAPI:        botAPI,
		self:          self,
		stopSignal:    make(chan bool),
	}
	reporter.Start(interval)

	return reporter
}

// Start begins reporting metrics according to the interval.
func (sr *serviceReporter) Start(interval time.Duration) {
	go func() {
		ticker := time.NewTicker(interval)
		for {
			select {
			case <-sr.stopSignal:
				return

			case <-ticker.C:
				sr.Report()
			}
		}
	}()
}

// Stop terminates metric reporting.
func (sr *serviceReporter) Stop() {
	sr.stopSignal <- true
}

// Use adds a new metric source to the reporter.
func (sr *serviceReporter) Use(botID int64, meter gom.Meter) {
	sr.meterLock.Lock()
	defer sr.meterLock.Unlock()

	sr.metersByBotID[botID] = meter
}

// Remove stops reporting metrics for the source identified by the id.
func (sr *serviceReporter) Remove(botID int64) {
	sr.meterLock.Lock()
	defer sr.meterLock.Unlock()

	delete(sr.metersByBotID, botID)
}

// Report immediately prepares and sends a report.
// This does not reset the established reporting interval.
func (sr *serviceReporter) Report() {
	sr.forEachBotWeight(func(botID int64, weight float64) {
		bot := &cluster.Bot{
			ID:     botID,
			Weight: weight,
		}
		if err := sr.botAPI.Save(bot, sr.self); err != nil {
			zap.S().Errorw("failed to save bot weight", "botId", botID, "error", err)
		}
	})
}

// asynchronously runs report on each bot and its weight
func (sr *serviceReporter) forEachBotWeight(report func(id int64, weight float64)) {
	sr.meterLock.Lock()
	defer sr.meterLock.Unlock()

	serviceWeight := 0.0
	for botID, meter := range sr.metersByBotID {
		weight := meter.Rate1()
		go report(botID, weight)
		serviceWeight += weight
	}
	sr.lastServiceWeight = serviceWeight
}

// Last returns the last recorded service weight.
func (sr *serviceReporter) LastServiceWeight() float64 {
	return sr.lastServiceWeight
}
