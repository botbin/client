package metrics_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	cmocks "gitlab.com/botbin/client/pkg/cluster/mocks"
	"gitlab.com/botbin/client/pkg/metrics"
	"gitlab.com/botbin/client/pkg/metrics/mocks"
)

type ServiceReporterTestSuite struct {
	suite.Suite
}

func (ss *ServiceReporterTestSuite) TestReport() {
	var botWeight float64

	botAPI := new(cmocks.BotAPI)
	botAPI.On("Save", mock.Anything, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		arg := args[0].(*cluster.Bot)
		botWeight = arg.Weight
	})

	const rate float64 = 10
	const id int64 = 1
	meter := new(mocks.Meter)
	meter.On("Rate1").Return(rate)

	interval := 5 * time.Millisecond
	reporter := metrics.NewReporter(interval, botAPI, cluster.Address("127.0.0.1:80"))
	reporter.Use(id, meter)
	
	time.Sleep(7 * time.Millisecond)
	reporter.Stop()

	ss.Equal(rate, botWeight)
	ss.Equal(rate, reporter.LastServiceWeight())

	botAPI.AssertExpectations(ss.T())
	meter.AssertExpectations(ss.T())
}

func TestServiceReporter(t *testing.T) {
	suite.Run(t, new(ServiceReporterTestSuite))
}
