package cluster

import "github.com/samuel/go-zookeeper/zk"

type zkState struct {
	conn     *zk.Conn
	rootNode string
}

// NewZKState creates a State that uses ZooKeeper.
func NewZKState(conn *zk.Conn, rootNode string) State {
	return &zkState{
		conn:     conn,
		rootNode: rootNode,
	}
}

// GetClientAPI creates and returns a new ClientAPI.
func (zcs *zkState) GetClientAPI() (ClientAPI, error) {
	return NewZKClientAPI(zcs.conn, zcs.rootNode)
}

// GetBotAPI creates and returns a new BotAPI.
func (zcs *zkState) GetBotAPI() (BotAPI, error) {
	return NewZKBotAPI(zcs.conn, zcs.rootNode)
}

// GetElectionAPI creates and returns a new ElectionAPI.
func (zcs *zkState) GetElectionAPI() (ElectionAPI, error) {
	return NewZKElectionAPI(zcs.conn, zcs.rootNode)
}

// GetQueueAPI creates and returns a new QueueAPI.
func (zcs *zkState) GetQueueAPI() (QueueAPI, error) {
	return NewZKQueueAPI(zcs.conn, zcs.rootNode)
}
