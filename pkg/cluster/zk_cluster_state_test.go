package cluster_test

import (
	"testing"

	"github.com/samuel/go-zookeeper/zk"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/data"
	"gitlab.com/botbin/client/pkg/util"
)

// Tests zkState
type ZKStateTestSuite struct {
	suite.Suite
	conn     *zk.Conn
	rootNode string
}

func (zs *ZKStateTestSuite) SetupSuite() {
	conn, err := data.GetZK()
	zs.Nil(err)

	zs.conn = conn
	zs.rootNode = "/"
}

func (zs *ZKStateTestSuite) TestGetClientAPI() {
	state := cluster.NewZKState(zs.conn, zs.rootNode)
	api, err := state.GetClientAPI()
	zs.NotNil(api)
	zs.Nil(err)
}

func (zs *ZKStateTestSuite) TestGetBotAPI() {
	state := cluster.NewZKState(zs.conn, zs.rootNode)
	api, err := state.GetBotAPI()
	zs.NotNil(api)
	zs.Nil(err)
}

func (zs *ZKStateTestSuite) TestGetElectionAPI() {
	state := cluster.NewZKState(zs.conn, zs.rootNode)
	api, err := state.GetElectionAPI()
	zs.NotNil(api)
	zs.Nil(err)
}

func (zs *ZKStateTestSuite) TestGetQueueAPI() {
	state := cluster.NewZKState(zs.conn, zs.rootNode)
	api, err := state.GetQueueAPI()
	zs.NotNil(api)
	zs.Nil(err)
}

func TestZKState(t *testing.T) {
	if util.GetTestMode() == util.TestingAll {
		suite.Run(t, new(ZKStateTestSuite))
	}
}
