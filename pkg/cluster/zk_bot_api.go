package cluster

import (
	"encoding/json"
	"strconv"

	"github.com/samuel/go-zookeeper/zk"
	"gitlab.com/botbin/client/pkg/data"
)

// zkBotAPI uses ZooKeeper to manage the global collection of bots that
// should be running.
type zkBotAPI struct {
	conn          *zk.Conn
	discoveryNode string
	botsNode      string
}

// NewZKBotAPI creates a BotAPI that uses ZooKeeper.
func NewZKBotAPI(conn *zk.Conn, rootNode string) (BotAPI, error) {
	if rootNode == "/" {
		rootNode = ""
	}

	api := &zkBotAPI{
		conn:          conn,
		discoveryNode: rootNode + "/discovery",
		botsNode:      rootNode + "/bots",
	}

	if err := data.CreateZNodePath(api.discoveryNode, conn); err != nil {
		return api, err
	}
	err := data.CreateZNodePath(api.botsNode, conn)
	return api, err
}

// GetLocation finds the client where a bot is currently running.
func (zba *zkBotAPI) GetLocation(botID int64) (ClientFacade, error) {
	node := zba.getNodeName(botID)
	clientAddress, _, err := zba.conn.Get(node)
	if err != nil {
		return nil, err
	}
	return newClient(Address(clientAddress)), nil
}

// SetLocation stores the client location of a bot.
func (zba *zkBotAPI) SetLocation(botID int64, location Address) error {
	_, err := zba.conn.Create(
		zba.getNodeName(botID),
		[]byte(location),
		zk.FlagEphemeral,
		zk.WorldACL(zk.PermAll),
	)
	return err
}

// RemoveLocation removes the bot-client location mapping if it exists.
func (zba *zkBotAPI) RemoveLocation(botID int64) error {
	node := zba.getNodeName(botID)
	return zba.conn.Delete(node, 0)
}

func (zba *zkBotAPI) getNodeName(botID int64) string {
	return zba.discoveryNode + "/" + strconv.FormatInt(botID, 10)
}

// Save associates a bot's metadata with its location.
//
// By saving metadata, we can make restarting the bot easier if
// its client fails.
func (zba *zkBotAPI) Save(bot *Bot, location Address) error {
	parentPath := zba.getBotParentPath(location)
	nodePath := zba.getBotPath(parentPath, bot.ID)

	nodeVal, err := json.Marshal(bot)
	if err != nil {
		return err
	}
	
	if _, stat, err := zba.conn.Get(nodePath); err != nil {
		return zba.createNewMetaNode(parentPath, nodePath, nodeVal)
	} else {
		return zba.updateMetaNode(nodePath, nodeVal, stat.Version)
	}
}

func (zba *zkBotAPI) createNewMetaNode(parentPath, nodePath string, value []byte) error {
	// TODO: This is going to become really ineffecient.
	//
	// Problem:
	// This is called often, sending requests to ZK that we can avoid.
	//
	// Solution:
	// We could keep a local cache of every parent node that is updated
	// using a watch on the bots node. Then we check the cache to decide
	// if we should create the parent path.
	if err := zba.tryCreateBotParentPath(parentPath); err != nil {
		return err
	}

	_, err := zba.conn.Create(nodePath, value, 0, zk.WorldACL(zk.PermAll))
	return err
}

func (zba *zkBotAPI) updateMetaNode(nodePath string, value []byte, version int32) error {
	_, err := zba.conn.Set(nodePath, value, version)
	return err
}

// creates the parent path of a bot node if the path does not exist
func (zba *zkBotAPI) tryCreateBotParentPath(parentPath string) error {
	if exists, _, err := zba.conn.Exists(parentPath); err != nil {
		return err
	} else if !exists {
		_, err := zba.conn.Create(parentPath, nil, 0, zk.WorldACL(zk.PermAll))
		if err != nil {
			return err
		}
	}
	return nil
}

// Remove clears bot metadata, disassociating it with the address.
//
// This does not stop the bot. It only removes metadata. The ClientFacade
// implementation supports stopping bots at remote clients.
func (zba *zkBotAPI) Remove(botID int64, location Address) error {
	parentPath := zba.getBotParentPath(location)
	botPath := zba.getBotPath(parentPath, botID)
	_, stat, err := zba.conn.Get(botPath)
	if err != nil {
		return err
	}
	return zba.conn.Delete(botPath, stat.Version)
}

func (zba *zkBotAPI) getBotParentPath(location Address) string {
	return zba.botsNode + "/" + string(location)
}

func (zba *zkBotAPI) getBotPath(parentPath string, botID int64) string {
	return parentPath + "/" + strconv.FormatInt(botID, 10)
}
