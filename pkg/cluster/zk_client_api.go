package cluster

import (
	"github.com/samuel/go-zookeeper/zk"
	"gitlab.com/botbin/client/pkg/data"
)

// zkClientAPI uses ZooKeeper to manage the global state of clients.
type zkClientAPI struct {
	conn        *zk.Conn
	clientsNode string
	botsNode    string
}

// NewZKClientAPI creates a ClientAPI that uses ZooKeeper.
func NewZKClientAPI(conn *zk.Conn, rootNode string) (ClientAPI, error) {
	if rootNode == "/" {
		rootNode = ""
	}

	api := &zkClientAPI{
		conn:        conn,
		clientsNode: rootNode + "/clients",
		botsNode:    rootNode + "/bots",
	}

	if err := data.CreateZNodePath(api.clientsNode, conn); err != nil {
		return api, err
	}
	err := data.CreateZNodePath(api.botsNode, conn)
	return api, err
}

// GetRunningClients returns all responsive clients.
func (zca *zkClientAPI) GetRunningClients() ([]ClientFacade, error) {
	var clients []ClientFacade
	err := zca.visitRunningClients(func(address string) {
		client := newClient(Address(address))
		clients = append(clients, client)
	})
	return clients, err
}

// GetStoppedClients returns all clients that are stopped and have yet
// to be cleaned up.
//
// This method is especially important in cases where the leader terminates
// before finishing the cleanup process.
func (zca *zkClientAPI) GetStoppedClients() ([]Address, error) {
	liveClients := make(map[string]bool)
	zca.visitRunningClients(func(address string) {
		liveClients[address] = true
	})

	children, _, err := zca.conn.Children(zca.botsNode)
	if err != nil {
		return nil, err
	}

	var stoppedClients []Address
	for _, address := range children {
		if _, exists := liveClients[address]; !exists {
			stoppedClients = append(stoppedClients, Address(address))
		}
	}
	return stoppedClients, nil
}

// calls visit with the address of each client that is responsive.
func (zca *zkClientAPI) visitRunningClients(visit func(address string)) error {
	children, _, err := zca.conn.Children(zca.clientsNode)
	if err != nil {
		return err
	}

	for _, address := range children {
		visit(address)
	}
	return nil
}

// Register registers this client in the cluster.
func (zca *zkClientAPI) Register(self Address) error {
	path := zca.clientsNode + "/" + string(self)
	_, err := zca.conn.Create(
		path,
		nil,
		zk.FlagEphemeral,
		zk.WorldACL(zk.PermAll),
	)
	return err
}
