package cluster

// Bot defines a bot instance in the context of the cluster state.
//
// This will often not represent the entire picture. It is kept minimal
// to reduce duplicating data from the main bot service and also to reduce
// the number of locations that hold secrets.
type Bot struct {
	// ID is the unique ID of a bot.
	ID int64 `json:"id"`

	// Config is the configuration that dictates how the bot will function.
	//
	// This value should not be printed in JSON content because the bot object
	// may be stored in a ZooKeeper node, which has a size limitation.
	Config string `json:"-"`

	// Token is the bot token uses to connect to Discord.
	//
	// This value should not be printed in JSON content because it is sensitive
	// in nature and should exist in as few places as possible. Ideally,
	// it should only be retrievable through the bot API.
	Token string `json:"-"`

	// Weight indicates the resource usage of the bot.
	Weight float64 `json:"weight"`
}

// QueuedBot defines a bot that is queued for assignment to a client.
//
// Bots are put in a queue prior to being assigned to a new client.
// This applies even during reassignment.
type QueuedBot struct {
	Bot

	// ClientAddress indicates where the bot is located.
	// This may be nil if the bot is being newly assigned to a client.
	ClientAddress Address `json:"clientAddress"`
}
