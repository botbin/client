package cluster_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/botbin/client/pkg/cluster"
)

func TestAddress(t *testing.T) {
	addr := cluster.Address("127.0.0.1:8080")
	assert.Equal(t, "127.0.0.1", addr.Host())
	assert.Equal(t, "8080", addr.Port())
}
