package cluster_test

import (
	"math"
	"strconv"
	"testing"
	"time"

	"github.com/samuel/go-zookeeper/zk"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/data"
	"gitlab.com/botbin/client/pkg/util"
)

// Tests zkElectionAPI
type ZKElectionAPITestSuite struct {
	suite.Suite
	conn *zk.Conn
	acl  []zk.ACL
}

func (zs *ZKElectionAPITestSuite) SetupSuite() {
	conn, err := data.GetZK()
	zs.Nil(err)

	zs.conn = conn
	zs.acl = zk.WorldACL(zk.PermAll)
	data.DeleteZNodeRecursive("/election", zs.conn)
}

func (zs *ZKElectionAPITestSuite) TearDownTest() {
	data.DeleteZNodeRecursive("/election", zs.conn)
}

func (zs *ZKElectionAPITestSuite) TestGetLeader_Exists() {
	api, err := cluster.NewZKElectionAPI(zs.conn, "/")
	zs.Nil(err)

	leaderAddr := cluster.Address("127.0.0.1:80")
	zs.createLeaderCandidate(leaderAddr)
	zs.createLeaderCandidate(cluster.Address("127.0.0.2:80"))

	leader, err := api.GetLeader()
	zs.Nil(err)
	zs.Equal(leaderAddr, leader.GetAddress())
}

func (zs *ZKElectionAPITestSuite) TestGetLeader_Missing() {
	api, err := cluster.NewZKElectionAPI(zs.conn, "/")
	zs.Nil(err)

	_, err = api.GetLeader()
	zs.NotNil(err)
}

func (zs *ZKElectionAPITestSuite) createLeaderCandidate(addr cluster.Address) {
	_, err := zs.conn.Create(
		"/election/",
		[]byte(addr),
		zk.FlagEphemeral|zk.FlagSequence,
		zs.acl,
	)
	zs.Nil(err)
}

func (zs *ZKElectionAPITestSuite) TestJoinCandidatePool_FirstClient() {
	api, err := cluster.NewZKElectionAPI(zs.conn, "/")
	zs.Nil(err)

	address := cluster.Address("127.0.0.1:80")
	elected := make(chan bool)
	err = api.JoinCandidatePool(address, func() {
		go func() { elected <- true }()
	})
	zs.Nil(err)

	select {
	case <-time.NewTimer(time.Second).C:
		zs.Fail("timed out")

	case <-elected:
	}
}

func (zs *ZKElectionAPITestSuite) TestJoinCandidatePool_SecondClient() {
	apiA, err := cluster.NewZKElectionAPI(zs.conn, "/")
	zs.Nil(err)

	terminateLeader := make(chan bool)

	leaderAddr := cluster.Address("127.0.0.1:80")
	err = apiA.JoinCandidatePool(leaderAddr, func() {
		go func() {
			<-terminateLeader
			zs.deleteLeaderNode()
		}()
	})
	zs.Nil(err)

	apiB, err := cluster.NewZKElectionAPI(zs.conn, "/")
	zs.Nil(err)

	nextLeaderAddr := cluster.Address("127.0.0.2:80")
	nextLeaderElected := make(chan bool)
	err = apiB.JoinCandidatePool(nextLeaderAddr, func() {
		nextLeaderElected <- true
	})
	zs.Nil(err)

	terminateLeader <- true

	select {
	case <-time.NewTimer(time.Second).C:
		zs.Fail("timed out")

	case <-nextLeaderElected:
	}
}

func (zs *ZKElectionAPITestSuite) deleteLeaderNode() {
	children, _, err := zs.conn.Children("/election")
	zs.Nil(err)

	minSeq := math.MaxInt32
	minChild := ""
	for _, child := range children {
		num, _ := strconv.Atoi(child)
		if num < minSeq {
			minSeq = num
			minChild = child
		}
	}
	data.DeleteZNodeRecursive("/election/"+minChild, zs.conn)
}

func TestZKElectionAPI(t *testing.T) {
	if util.GetTestMode() == util.TestingAll {
		suite.Run(t, new(ZKElectionAPITestSuite))
	}
}
