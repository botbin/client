package cluster_test

import (
	"encoding/json"
	"math/rand"
	"sort"
	"strconv"
	"testing"
	"time"

	"github.com/samuel/go-zookeeper/zk"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/data"
	"gitlab.com/botbin/client/pkg/util"
)

// Tests zkQueueAPI
type ZKQueueAPITestSuite struct {
	suite.Suite
	conn *zk.Conn
	acl  []zk.ACL
}

func (zs *ZKQueueAPITestSuite) SetupSuite() {
	conn, err := data.GetZK()
	zs.Nil(err)

	zs.conn = conn
	zs.acl = zk.WorldACL(zk.PermAll)
	data.DeleteZNodeRecursive("/queue", zs.conn)
}

func (zs *ZKQueueAPITestSuite) TearDownTest() {
	data.DeleteZNodeRecursive("/queue", zs.conn)
}

func (zs *ZKQueueAPITestSuite) TestQueue_Exists() {
	api, err := cluster.NewZKQueueAPI(zs.conn, "/")
	zs.Nil(err)

	_, err = zs.conn.Create("/queue/1", nil, 0, zs.acl)
	zs.Nil(err)
	defer data.DeleteZNodeRecursive("/queue/1", zs.conn)

	err = api.Queue(&cluster.QueuedBot{Bot: cluster.Bot{ID: 1}})
	zs.NotNil(err)
}

func (zs *ZKQueueAPITestSuite) TestQueue_NotExists() {
	api, err := cluster.NewZKQueueAPI(zs.conn, "/")
	zs.Nil(err)

	expectedBot := &cluster.QueuedBot{
		Bot: cluster.Bot{
			ID:     1,
			Weight: 3.2,
		},
		ClientAddress: cluster.Address("127.0.0.1:80"),
	}
	err = api.Queue(expectedBot)
	zs.Nil(err)

	d, _, err := zs.conn.Get("/queue/1")
	zs.Nil(err)

	actualBot := new(cluster.QueuedBot)
	zs.Nil(json.Unmarshal(d, actualBot))
	zs.Equal(expectedBot.ID, actualBot.ID)
	zs.Equal(expectedBot.ClientAddress, actualBot.ClientAddress)
	zs.Equal(expectedBot.Weight, actualBot.Weight)
}

func (zs *ZKQueueAPITestSuite) TestGetAll_NotEmpty() {
	api, err := cluster.NewZKQueueAPI(zs.conn, "/")
	zs.Nil(err)

	expected := []*cluster.QueuedBot{
		&cluster.QueuedBot{Bot: cluster.Bot{ID: 1}},
		&cluster.QueuedBot{Bot: cluster.Bot{ID: 2}},
	}
	api.Queue(expected[0])
	api.Queue(expected[1])

	actual, err := api.GetAll()
	zs.Nil(err)
	zs.Len(actual, 2)

	c := 0
	if expected[0].ID == actual[0].ID {
		c++
		if expected[1].ID == actual[1].ID {
			c++
		}
	} else if expected[0].ID == actual[1].ID {
		c++
		if expected[1].ID == actual[0].ID {
			c++
		}
	}
	zs.Equal(2, c)
}

func (zs *ZKQueueAPITestSuite) TestGetAll_Empty() {
	api, err := cluster.NewZKQueueAPI(zs.conn, "/")
	zs.Nil(err)

	bots, err := api.GetAll()
	zs.Nil(err)
	zs.Len(bots, 0)
}

func (zs *ZKQueueAPITestSuite) TestQueueAll() {
	zs.runQueueAllTests([]queueAllTestCase{
		queueAllTestCase{
			ClientBots:   []*cluster.Bot{},
			ClientIsLive: false,
		},
		queueAllTestCase{
			ClientBots:   []*cluster.Bot{&cluster.Bot{ID: 1, Weight: 3.2}},
			ClientIsLive: true,
		},
		queueAllTestCase{
			ClientBots:   []*cluster.Bot{&cluster.Bot{ID: 1, Weight: 3.2}},
			ClientIsLive: false,
		},
		queueAllTestCase{
			ClientBots: []*cluster.Bot{
				&cluster.Bot{ID: 1, Weight: 3.002},
				&cluster.Bot{ID: 2, Weight: 0.0},
				&cluster.Bot{ID: 3, Weight: 100000.0},
			},
			ClientIsLive: true,
		},
	})
}

// runs a collection of test cases against the QueueAll method
func (zs *ZKQueueAPITestSuite) runQueueAllTests(cases []queueAllTestCase) {
	addr := cluster.Address("127.0.0.1:80")
	for _, c := range cases {
		api, err := cluster.NewZKQueueAPI(zs.conn, "/")
		zs.Nil(err)

		bots, err := api.GetAll()
		zs.Nil(err)
		zs.Empty(bots)

		// Add bots to the client node so that QueueAll can pull them from there.
		data.CreateZNodePath("/bots/"+string(addr), zs.conn)
		for _, bot := range c.ClientBots {
			val, err := json.Marshal(bot)
			zs.Nil(err)
			_, err = zs.conn.Create("/bots/"+string(addr)+"/"+strconv.FormatInt(bot.ID, 10), val, 0, zs.acl)
			zs.Nil(err)
		}
		zs.Nil(api.QueueAll(addr, c.ClientIsLive))

		bots, err = api.GetAll()
		zs.Nil(err)
		zs.Equal(len(c.ClientBots), len(bots))

		zs.sort(c.ClientBots)
		zs.sort(bots)
		for i := 0; i < len(bots); i++ {
			zs.Equal(c.ClientBots[i].ID, bots[i].ID)
			zs.Equal(c.ClientBots[i].Weight, bots[i].Weight)
			zs.Empty(bots[i].Token)
			zs.Empty(bots[i].Config)
			if c.ClientIsLive {
				zs.Equal(addr, bots[i].ClientAddress)
			} else {
				zs.Equal("", string(bots[i].ClientAddress))
			}
		}
		zs.Nil(data.DeleteZNodeRecursive("/queue", zs.conn))
		zs.Nil(data.DeleteZNodeRecursive("/bots", zs.conn))
	}
}

// sorts a collection of *QueuedBot or *Bot according to ID
func (zs *ZKQueueAPITestSuite) sort(bots interface{}) {
	switch a := bots.(type) {
	case []*cluster.Bot:
		sort.Slice(bots, func(i, j int) bool { return a[i].ID < a[j].ID })
	case []*cluster.QueuedBot:
		sort.Slice(bots, func(i, j int) bool { return a[i].ID < a[j].ID })
	}
}

type queueAllTestCase struct {
	ClientBots   []*cluster.Bot
	ClientIsLive bool
}

func (zs *ZKQueueAPITestSuite) TestDequeue_Exists() {
	api, err := cluster.NewZKQueueAPI(zs.conn, "/")
	zs.Nil(err)

	bot := &cluster.QueuedBot{
		Bot: cluster.Bot{ID: 1},
	}
	zs.Nil(api.Queue(bot))

	bots, err := api.GetAll()
	zs.Nil(err)
	zs.Equal(int64(1), bots[0].ID)

	zs.Nil(api.Dequeue(bot.ID))

	bots, err = api.GetAll()
	zs.Nil(err)
	zs.Empty(bots)
}

func (zs *ZKQueueAPITestSuite) TestDequeue_Missing() {
	api, err := cluster.NewZKQueueAPI(zs.conn, "/")
	zs.Nil(err)

	zs.NotNil(api.Dequeue(rand.Int63()))
}

func TestZKQueueAPI(t *testing.T) {
	if util.GetTestMode() == util.TestingAll {
		rand.Seed(time.Now().Unix())
		suite.Run(t, new(ZKQueueAPITestSuite))
	}
}
