package cluster

import (
	"context"

	"gitlab.com/botbin/client/pkg/service/serial"
	"google.golang.org/grpc"
)

// ClientFacade provides a way to interact with another client in the cluster.
type ClientFacade interface {
	// StartBot assigns a bot to the client.
	StartBot(bot *Bot) error

	// StopBot terminates a bot in the client.
	StopBot(id int64) error

	// UpdateBot updates the settings of a bot in the client.
	UpdateBot(bot *Bot) error

	// Terminate signals to the client that it should shut down once all of
	// its bots have been stopped.
	Terminate(reason string) error

	// IsTerminating returns true if the Terminate signal was given but the
	// client still contains bots.
	IsTerminating() (bool, error)

	// ForceTerminate forcibly shuts down the client without first reassigning
	// its bots.
	ForceTerminate(reason string) error

	// GetAddress returns the address where the client is accepting requests.
	GetAddress() Address

	// GetWeight returns the current weight of the client.
	//
	// The weight of a client is the sum of the weights of its bots.
	GetWeight() (float64, error)

	// Connect opens a connection to the client.
	Connect() error

	// Disconnect closes the connection to the client.
	Disconnect() error
}

type clientFacade struct {
	addr   Address
	conn   *grpc.ClientConn
	client serial.ClientClient
}

// newClient creates a new ClientFacade.
// This method does not call Connect().
func newClient(addr Address) ClientFacade {
	return &clientFacade{
		addr: addr,
	}
}

// StartBot assigns a bot to the client.
func (cf *clientFacade) StartBot(bot *Bot) error {
	_, err := cf.client.Start(context.Background(), &serial.StartRequest{
		Id:    bot.ID,
		Token: bot.Token,
		Config: &serial.Config{
			Content: bot.Config,
		},
		Weight: bot.Weight,
	})
	return err
}

// StopBot terminates a bot in the client.
func (cf *clientFacade) StopBot(id int64) error {
	_, err := cf.client.Stop(context.Background(), &serial.StopRequest{
		Id: id,
	})
	return err
}

// UpdateBot updates the settings of a bot in the client.
func (cf *clientFacade) UpdateBot(bot *Bot) error {
	_, err := cf.client.Update(context.Background(), &serial.UpdateRequest{
		Id: bot.ID,
		Config: &serial.Config{
			Content: bot.Config,
		},
	})
	return err
}

// Terminate signals to the client that it should shut down once all of
// its bots have been stopped.
func (cf *clientFacade) Terminate(reason string) error {
	_, err := cf.client.Terminate(context.Background(), &serial.TerminateRequest{
		Reason: reason,
	})
	return err
}

// IsTerminating returns true if the Terminate signal was given but the
// client still contains bots.
func (cf *clientFacade) IsTerminating() (bool, error) {
	res, err := cf.client.GetState(context.Background(), new(serial.GetStateRequest))
	if err != nil {
		return false, err
	}
	return res.GetState() == serial.ClientState_Terminating, nil
}

// ForceTerminate forcibly shuts down the client without first reassigning
// its bots.
func (cf *clientFacade) ForceTerminate(reason string) error {
	_, err := cf.client.ForceTerminate(context.Background(), &serial.TerminateRequest{
		Reason: reason,
	})
	return err
}

// GetAddress returns the address where the client is accepting requests.
func (cf *clientFacade) GetAddress() Address {
	return cf.addr
}

// GetWeight returns the current weight of the client.
//
// The weight of a client is the sum of the weights of its bots.
func (cf *clientFacade) GetWeight() (float64, error) {
	weight, err := cf.client.GetWeight(context.Background(), new(serial.GetWeightRequest))
	if err != nil {
		return 0.0, err
	}
	return weight.GetWeight(), nil
}

// Connect opens a connection to the client.
func (cf *clientFacade) Connect() error {
	conn, err := grpc.Dial(string(cf.addr), grpc.WithInsecure())
	if err == nil {
		cf.conn = conn
		cf.client = serial.NewClientClient(conn)
	}
	return err
}

// Disconnect closes the connection to the client.
func (cf *clientFacade) Disconnect() error {
	return cf.conn.Close()
}
