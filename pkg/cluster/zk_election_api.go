package cluster

import (
	"errors"
	"math"
	"strconv"

	"github.com/samuel/go-zookeeper/zk"
	"gitlab.com/botbin/client/pkg/data"
	"go.uber.org/zap"
)

// zkElectionAPI uses ZooKeeper to implement leader election.
type zkElectionAPI struct {
	conn         *zk.Conn
	electionNode string

	// The sequence number of this client instance if it has
	// joined the election process.
	sequenceNum string
	// The address of this client
	self Address
}

// NewZKElectionAPI creates an ElectionAPI that uses ZooKeeper.
func NewZKElectionAPI(conn *zk.Conn, rootNode string) (ElectionAPI, error) {
	if rootNode == "/" {
		rootNode = ""
	}
	api := &zkElectionAPI{
		conn:         conn,
		electionNode: rootNode + "/election",
	}
	err := data.CreateZNodePath(api.electionNode, conn)
	return api, err
}

// GetLeader returns the current leader of the cluster.
func (zea *zkElectionAPI) GetLeader() (ClientFacade, error) {
	// TODO: Use a leader znode that is modified whenever a new leader is elected.
	// That would speed up this operation, but we would need to worry about updating
	// the node during the leader setup process.

	minNode, err := zea.getMinElectionNode()
	if err != nil {
		return nil, err
	}
	leaderNode := zea.electionNode + "/" + minNode
	return zea.getClient(leaderNode)
}

// gets the election node that belongs to the cluster leader
func (zea *zkElectionAPI) getMinElectionNode() (string, error) {
	children, err := zea.getElectionChildren()
	if err != nil {
		return "", err
	}

	if len(children) == 0 {
		return "", errors.New("cannot get leader; none exists")
	}
	return zea.findMinChild(children), nil
}

// returns the child with the minimum sequence number
func (zea *zkElectionAPI) findMinChild(electionChildren []string) string {
	minSeqNum := math.MaxInt32
	var minChild string

	for _, seqNum := range electionChildren {
		num, _ := strconv.Atoi(seqNum)
		if num < minSeqNum {
			minSeqNum = num
			minChild = seqNum
		}
	}
	return minChild
}

func (zea *zkElectionAPI) getClient(leaderNode string) (ClientFacade, error) {
	rawAddr, _, err := zea.conn.Get(leaderNode)
	if err != nil {
		return nil, err
	}
	return newClient(Address(rawAddr)), nil
}

// JoinCandidatePool makes the client eligible to become the cluster leader.
func (zea *zkElectionAPI) JoinCandidatePool(self Address, onElected func()) error {
	zea.self = self
	if err := zea.createCandidateNode(); err != nil {
		return err
	}

	children, err := zea.getElectionChildren()
	if err != nil {
		return err
	}

	if isLeader, err := zea.isLeader(children); err != nil {
		return err
	} else if isLeader {
		onElected()
		return nil
	}
	return zea.watchElection(children, onElected)
}

// allows this client to take part in future elections until it dies
func (zea *zkElectionAPI) createCandidateNode() error {
	seqNum, err := zea.conn.Create(
		zea.electionNode+"/",
		[]byte(zea.self),
		zk.FlagEphemeral|zk.FlagSequence,
		zk.WorldACL(zk.PermAll),
	)

	if err == nil {
		zea.sequenceNum = seqNum
	}
	return err
}

// watches future election processes and calls onElected() if this client becomes the leader
func (zea *zkElectionAPI) watchElection(electionChildren []string, onElected func()) error {
	predecessor, err := zea.getPredecessorNode(electionChildren)
	if err != nil {
		return err
	}
	predecessorPath := zea.electionNode + "/" + predecessor
	return zea.awaitPredecessorDeath(predecessorPath, onElected)
}

// gets the name of the node whose sequence number comes before the number of this client
func (zea *zkElectionAPI) getPredecessorNode(electionChildren []string) (string, error) {
	var predecessor string
	for i := 0; i < len(electionChildren)-1; i++ {
		addr, err := zea.getAddress(electionChildren[i+1])
		if err != nil {
			return "", err
		}

		if addr == zea.self {
			predecessor = electionChildren[i]
		}
	}

	if predecessor == "" {
		return "", errors.New("invalid operation; cannot get predecessor of leader node")
	}
	return predecessor, nil
}

// waits for a node, whose sequence number is less than this client, to die. If that node
// was the leader, onElected() is called.
func (zea *zkElectionAPI) awaitPredecessorDeath(predecessorPath string, onElected func()) error {
	_, _, predEvent, err := zea.conn.GetW(predecessorPath)
	if err != nil {
		return err
	}

	go func() {
		event := <-predEvent

		children, err := zea.getElectionChildren()
		if err != nil {
			zea.panic(err)
		}

		if event.Type == zk.EventNodeDeleted {
			if isLeader, err := zea.isLeader(children); err != nil {
				zea.panic(err)
			} else if isLeader {
				onElected()
				return
			}
		}

		if err := zea.watchElection(children, onElected); err != nil {
			zea.panic(err)
		}
	}()
	return nil
}

func (zea *zkElectionAPI) panic(err error) {
	zap.S().Panicw("failed to rejoin election", "error", err)
}

// returns true if this client instance is the leader
// electionChildren are the child nodes of the election node.
func (zea *zkElectionAPI) isLeader(electionChildren []string) (bool, error) {
	minChild := zea.findMinChild(electionChildren)
	childAddr, err := zea.getAddress(minChild)
	if err != nil {
		return false, err
	}
	return childAddr == zea.self, nil
}

// gets the address of the client that created the election node
func (zea *zkElectionAPI) getAddress(electionChildNode string) (Address, error) {
	val, _, err := zea.conn.Get(zea.electionNode + "/" + electionChildNode)
	if err != nil {
		return "", err
	}
	return Address(val), nil
}

// returns the names of all nodes taking part in elections
func (zea *zkElectionAPI) getElectionChildren() ([]string, error) {
	children, _, err := zea.conn.Children(zea.electionNode)
	return children, err
}
