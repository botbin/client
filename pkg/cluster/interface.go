package cluster

// State represents the global state of the cluster.
//
// It consists of multiple APIs that permit querying and modifying
// data from different contexts.
type State interface {
	GetClientAPI() (ClientAPI, error)
	GetBotAPI() (BotAPI, error)
	GetElectionAPI() (ElectionAPI, error)
	GetQueueAPI() (QueueAPI, error)
}

// ClientAPI provides access to the global state of all clients in the cluster.
type ClientAPI interface {
	// GetRunningClients returns all responsive clients.
	GetRunningClients() ([]ClientFacade, error)

	// GetStoppedClients returns all clients that are stopped and have yet
	// to be cleaned up.
	//
	// This method is especially important in cases where the leader terminates
	// before finishing the cleanup process.
	GetStoppedClients() ([]Address, error)

	// Register registers this client in the cluster.
	//
	// Registration should be done through some ephemeral data point. By doing
	// so, we allow the cluster leader to detect when clients fail.
	Register(self Address) error
}

// BotAPI provides access to the global state of all bots in the cluster.
type BotAPI interface {
	// GetLocation finds the client where a bot is currently running.
	GetLocation(id int64) (ClientFacade, error)

	// SetLocation stores the client location of a bot.
	//
	// This permits service discovery through GetLocation. It is important
	// that, should a client fail unexpectedly, the discovery data of a
	// bot is removed. If the data store implementation supports ephemeral
	// key-value pairs, this would be a good use of them.
	SetLocation(id int64, location Address) error

	// RemoveLocation removes the bot-client location mapping if it exists.
	RemoveLocation(botID int64) error

	// Save associates a bot's metadata with its location.
	//
	// By saving metadata, we can make restarting the bot easier if
	// its client fails.
	Save(bot *Bot, location Address) error

	// Remove clears bot metadata, disassociating it with the address.
	//
	// This does not stop the bot. It only removes metadata. The ClientFacade
	// implementation supports stopping bots at remote clients.
	Remove(botID int64, location Address) error
}

// ElectionAPI provides access to cluster leader elections.
type ElectionAPI interface {
	// GetLeader returns the current leader of the cluster.
	GetLeader() (ClientFacade, error)

	// JoinCandidatePool makes the client eligible to become the cluster leader.
	JoinCandidatePool(self Address, onElected func()) error
}

// QueueAPI permits the queueing of bots that should be assigned to new clients.
type QueueAPI interface {
	// GetAll returns all bots that have been queued but not yet assigned
	// to a new client.
	GetAll() ([]*QueuedBot, error)

	// Queue adds a bot to the assignment queue if it does not already exist.
	Queue(bot *QueuedBot) error

	// QueueAll adds each bot held by a client to the assignment queue.
	//
	// If the provided address does not point to a live client, the bots
	// it held prior to termination will be queued. If the client is live,
	// its bots are queued but not stopped.
	QueueAll(clientAddr Address, clientIsLive bool) error

	// Dequeue removes a bot from the queue.
	Dequeue(botID int64) error
}
