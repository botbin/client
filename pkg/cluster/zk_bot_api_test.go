package cluster_test

import (
	"encoding/json"
	"testing"

	"github.com/samuel/go-zookeeper/zk"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/data"
	"gitlab.com/botbin/client/pkg/util"
)

// Tests zkBotAPI
type ZKBotAPITestSuite struct {
	suite.Suite
	conn *zk.Conn
	acl  []zk.ACL
}

func (zs *ZKBotAPITestSuite) SetupSuite() {
	conn, err := data.GetZK()
	zs.Nil(err)

	zs.conn = conn
	zs.acl = zk.WorldACL(zk.PermAll)
	data.DeleteZNodeRecursive("/discovery", zs.conn)
	data.DeleteZNodeRecursive("/bots", zs.conn)
}

func (zs *ZKBotAPITestSuite) TearDownTest() {
	data.DeleteZNodeRecursive("/discovery", zs.conn)
	data.DeleteZNodeRecursive("/bots", zs.conn)
}

func (zs *ZKBotAPITestSuite) TestGetLocation_Exists() {
	api, err := cluster.NewZKBotAPI(zs.conn, "/")
	zs.Nil(err)

	addr := []byte("127.0.0.1:8080")
	_, err = zs.conn.Create("/discovery/1", addr, zk.FlagEphemeral, zs.acl)
	zs.Nil(err)

	client, err := api.GetLocation(1)
	zs.Nil(err)
	zs.Equal(cluster.Address(string(addr)), client.GetAddress())
}

func (zs *ZKBotAPITestSuite) TestGetLocation_Missing() {
	api, err := cluster.NewZKBotAPI(zs.conn, "/")
	zs.Nil(err)

	_, err = api.GetLocation(1)
	zs.NotNil(err)
}

func (zs *ZKBotAPITestSuite) TestSetLocation() {
	api, err := cluster.NewZKBotAPI(zs.conn, "/")
	zs.Nil(err)

	addr := cluster.Address("127.0.0.1:8080")
	var id int64 = 1
	err = api.SetLocation(id, addr)
	zs.Nil(err)

	client, err := api.GetLocation(id)
	zs.Nil(err)
	zs.Equal(addr, client.GetAddress())
}

func (zs *ZKBotAPITestSuite) TestRemoveLocation() {
	api, err := cluster.NewZKBotAPI(zs.conn, "/")
	zs.Nil(err)

	addr := cluster.Address("127.0.0.1:8080")
	var id int64 = 1
	err = api.SetLocation(id, addr)
	zs.Nil(err)

	client, err := api.GetLocation(id)
	zs.Nil(err)
	zs.Equal(addr, client.GetAddress())

	err = api.RemoveLocation(id)
	zs.Nil(err)

	_, err = api.GetLocation(id)
	zs.NotNil(err)
}

func (zs *ZKBotAPITestSuite) TestSave() {
	api, err := cluster.NewZKBotAPI(zs.conn, "/")
	zs.Nil(err)

	addr := cluster.Address("127.0.0.1:8080")
	// Use two bots. The root node won't exist on the first call, but
	// it will on the second.
	bots := []*cluster.Bot{
		&cluster.Bot{ID: 1, Weight: 1.0},
		&cluster.Bot{ID: 2, Weight: 2.0},
	}

	zs.Nil(api.Save(bots[0], addr))
	zs.Nil(api.Save(bots[1], addr))

	zs.compareNodeValue(bots[0], "/bots/127.0.0.1:8080/1", true)
	zs.compareNodeValue(bots[1], "/bots/127.0.0.1:8080/2", true)
}

func (zs *ZKBotAPITestSuite) TestRemove_Exists() {
	api, err := cluster.NewZKBotAPI(zs.conn, "/")
	zs.Nil(err)

	addr := cluster.Address("127.0.0.1:8080")
	bot := &cluster.Bot{ID: 1, Weight: 1.0}
	zs.Nil(api.Save(bot, addr))

	zs.compareNodeValue(bot, "/bots/127.0.0.1:8080/1", true)
	zs.Nil(api.Remove(bot.ID, addr))
	zs.compareNodeValue(nil, "/bots/127.0.0.1:8080/1", false)
}

func (zs *ZKBotAPITestSuite) compareNodeValue(expected *cluster.Bot, nodePath string, shouldExist bool) {
	val, _, err := zs.conn.Get(nodePath)
	if shouldExist {
		zs.Nil(err)
	} else {
		zs.NotNil(err)
		return
	}

	actual := new(cluster.Bot)
	zs.Nil(json.Unmarshal(val, actual))
	zs.EqualValues(expected, actual)
}

func TestZKBotAPI(t *testing.T) {
	if util.GetTestMode() == util.TestingAll {
		suite.Run(t, new(ZKBotAPITestSuite))
	}
}
