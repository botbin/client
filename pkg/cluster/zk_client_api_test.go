package cluster_test

import (
	"testing"

	"github.com/samuel/go-zookeeper/zk"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/client/pkg/cluster"
	"gitlab.com/botbin/client/pkg/data"
	"gitlab.com/botbin/client/pkg/util"
)

// Tests zkClientAPI
type ZKClientAPITestSuite struct {
	suite.Suite
	conn *zk.Conn
	acl  []zk.ACL
}

func (zs *ZKClientAPITestSuite) SetupSuite() {
	conn, err := data.GetZK()
	zs.Nil(err)

	zs.conn = conn
	zs.acl = zk.WorldACL(zk.PermAll)
	data.DeleteZNodeRecursive("/bots", zs.conn)
	data.DeleteZNodeRecursive("/clients", zs.conn)
}

func (zs *ZKClientAPITestSuite) TearDownTest() {
	data.DeleteZNodeRecursive("/bots", zs.conn)
	data.DeleteZNodeRecursive("/clients", zs.conn)
}

func (zs *ZKClientAPITestSuite) TestGetRunningClients() {
	api, err := cluster.NewZKClientAPI(zs.conn, "/")
	zs.Nil(err)

	_, err = zs.conn.Create("/clients/127.0.0.1:80", nil, zk.FlagEphemeral, zs.acl)
	zs.Nil(err)
	_, err = zs.conn.Create("/bots/127.0.0.2:80", nil, 0, zs.acl)
	zs.Nil(err)

	clients, err := api.GetRunningClients()
	zs.Nil(err)
	zs.Len(clients, 1)
	zs.Equal(cluster.Address("127.0.0.1:80"), clients[0].GetAddress())
}

func (zs *ZKClientAPITestSuite) TestGetStoppedClients() {
	api, err := cluster.NewZKClientAPI(zs.conn, "/")
	zs.Nil(err)

	_, err = zs.conn.Create("/bots/127.0.0.1:80", nil, 0, zs.acl)
	zs.Nil(err)
	_, err = zs.conn.Create("/clients/127.0.0.2:80", nil, zk.FlagEphemeral, zs.acl)
	zs.Nil(err)

	clients, err := api.GetStoppedClients()
	zs.Nil(err)
	zs.Len(clients, 1)
	zs.Equal(cluster.Address("127.0.0.1:80"), clients[0])
}

func (zs *ZKClientAPITestSuite) TestRegister() {
	api, err := cluster.NewZKClientAPI(zs.conn, "/")
	zs.Nil(err)

	address := cluster.Address("127.0.0.1:80")
	zs.Nil(api.Register(address))

	clients, err := api.GetRunningClients()
	zs.Nil(err)
	zs.Equal(address, clients[0].GetAddress())
}

func TestZKClientAPI(t *testing.T) {
	if util.GetTestMode() == util.TestingAll {
		suite.Run(t, new(ZKClientAPITestSuite))
	}
}
