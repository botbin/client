package cluster

import (
	"encoding/json"
	"strconv"

	"sync/atomic"

	"github.com/samuel/go-zookeeper/zk"
	"gitlab.com/botbin/client/pkg/data"
	"go.uber.org/zap"
)

// zkQueueAPI uses ZooKeeper to queue bots so that they may be started later.
type zkQueueAPI struct {
	conn      *zk.Conn
	queueNode string
	botsNode  string
}

// NewZKQueueAPI creates a QueueAPI that uses ZooKeeper.
func NewZKQueueAPI(conn *zk.Conn, rootNode string) (QueueAPI, error) {
	if rootNode == "/" {
		rootNode = ""
	}

	api := &zkQueueAPI{
		conn:      conn,
		queueNode: rootNode + "/queue",
		botsNode:  rootNode + "/bots",
	}

	if err := data.CreateZNodePath(api.queueNode, conn); err != nil {
		return api, err
	}
	err := data.CreateZNodePath(api.botsNode, conn)
	return api, err
}

// GetAll returns all bots that have been queued but not yet assigned
// to a new client.
func (zqa *zkQueueAPI) GetAll() ([]*QueuedBot, error) {
	children, _, err := zqa.conn.Children(zqa.queueNode)
	if err != nil {
		return nil, err
	}

	bots := []*QueuedBot{}
	for bot := range zqa.getQueuedBots(children) {
		bots = append(bots, bot)
	}
	return bots, nil
}

// gets all queued bots, which are sent over a channel when retrieved
// The returned channel is closed when all requests complete.
func (zqa *zkQueueAPI) getQueuedBots(botIDs []string) <-chan *QueuedBot {
	res := make(chan *QueuedBot)
	remaining := int64(len(botIDs))
	if remaining == 0 {
		close(res)
		return res
	}

	for _, botID := range botIDs {
		path := zqa.queueNode + "/" + botID
		go zqa.getQueuedBot(path, &remaining, res)
	}
	return res
}

func (zqa *zkQueueAPI) getQueuedBot(path string, remainingRequests *int64, res chan<- *QueuedBot) {
	val, _, err := zqa.conn.Get(path)
	if err != nil {
		zap.S().Errorw("failed to get queue node", "error", err)
		return
	}

	bot := new(QueuedBot)
	if err := json.Unmarshal(val, bot); err != nil {
		zap.S().Errorw("failed to unmarshal queue node data", "error", err)
		return
	}
	res <- bot

	if atomic.AddInt64(remainingRequests, -1) == 0 {
		close(res)
	}
}

// Queue adds a bot to the assignment queue if it does not already exist.
func (zqa *zkQueueAPI) Queue(bot *QueuedBot) error {
	val, err := json.Marshal(bot)
	if err != nil {
		return err
	}

	node := zqa.getQueuePath(bot.ID)
	_, err = zqa.conn.Create(node, val, 0, zk.WorldACL(zk.PermAll))
	return err
}

// QueueAll adds each bot held by a client to the assignment queue.
//
// If the provided address does not point to a live client, the bots
// it held prior to termination will be queued. If the client is live,
// its bots are queued but not stopped.
func (zqa *zkQueueAPI) QueueAll(clientAddr Address, clientIsLive bool) error {
	clientPath := zqa.botsNode + "/" + string(clientAddr)
	if !clientIsLive {
		clientAddr = ""
	}

	bots, err := zqa.getClientBots(clientPath, clientAddr)
	if err != nil {
		return err
	}

	requests := zqa.prepareCreateRequests(clientPath, bots)
	results, err := zqa.conn.Multi(requests...)
	if err != nil {
		return err
	}

	var firstResErr error
	for i := range results {
		if results[i].Error != nil {
			if firstResErr == nil {
				firstResErr = results[i].Error
			}
			zap.S().Errorw("failed to queue bot", "error", results[i].Error)
		}
	}
	return firstResErr
}

// extracts data from every bot node of a client
// If clientAddr is not "", it will be stored in each *QueuedBot.
// The returned channel is closed after the last bot is retrieved.
func (zqa *zkQueueAPI) getClientBots(clientPath string, clientAddr Address) (<-chan queuedBotResult, error) {
	children, _, err := zqa.conn.Children(clientPath)
	if err != nil {
		return nil, err
	}

	results := make(chan queuedBotResult)
	remaining := int64(len(children))
	if remaining == 0 {
		close(results)
		return results, nil
	}

	for _, botID := range children {
		go func(id string) {
			path := clientPath + "/" + id
			bot, err := zqa.getClientBot(path, clientAddr)

			results <- queuedBotResult{
				Bot: bot,
				Err: err,
			}

			if atomic.AddInt64(&remaining, -1) == 0 {
				close(results)
			}
		}(botID)
	}
	return results, nil
}

type queuedBotResult struct {
	Bot *QueuedBot
	Err error
}

// extracts data from a client's bots node
// If clientAddr is not "", it will be stored in each *QueuedBot.
func (zqa *zkQueueAPI) getClientBot(path string, clientAddr Address) (*QueuedBot, error) {
	val, _, err := zqa.conn.Get(path)
	if err != nil {
		return nil, err
	}

	bot := new(QueuedBot)
	err = json.Unmarshal(val, bot)
	if err == nil && clientAddr != "" {
		bot.ClientAddress = clientAddr
	}
	return bot, err
}

// creates a *zk.CreateRequest for each result that did not fail
func (zqa *zkQueueAPI) prepareCreateRequests(clientPath string, results <-chan queuedBotResult) []interface{} {
	var zkOps []interface{}
	var botErr error

	for bot := range results {
		if bot.Err != nil {
			botErr = bot.Err
		} else {
			if data, err := json.Marshal(bot.Bot); err != nil {
				botErr = err
			} else {
				zkOps = append(zkOps, &zk.CreateRequest{
					Path:  zqa.getQueuePath(bot.Bot.ID),
					Data:  data,
					Acl:   zk.WorldACL(zk.PermAll),
					Flags: 0,
				})
			}
		}

		if botErr != nil {
			zap.S().Errorw("failed to queue bot", "error", botErr)
			botErr = nil
		}
	}
	return zkOps
}

// Dequeue removes a bot from the queue.
func (zqa *zkQueueAPI) Dequeue(botID int64) error {
	node := zqa.getQueuePath(botID)
	return zqa.conn.Delete(node, 0)
}

// gets the znode path for a queued bot
func (zqa *zkQueueAPI) getQueuePath(botID int64) string {
	return zqa.queueNode + "/" + strconv.FormatInt(botID, 10)
}
