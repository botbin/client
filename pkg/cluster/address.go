package cluster

import (
	"strings"
)

// Address is a host:port string.
type Address string

func (a Address) Host() string {
	return strings.Split(string(a), ":")[0]
}

func (a Address) Port() string {
	return strings.Split(string(a), ":")[1]
}
