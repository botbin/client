package util

import (
	"os"
)

// TestMode indicates which type of tests should be run.
type TestMode int

const (
	// TestingUnit represents unit tests without any external dependencies.
	TestingUnit TestMode = iota
	// TestingAll represents all tests that have been written.
	TestingAll
)

// GetTestMode returns the current test mode.
// The value is read from the TEST_MODE environment variable.
func GetTestMode() TestMode {
	mode := os.Getenv("TEST_MODE")
	switch mode {
	case "unit", "":
		return TestingUnit

	case "all":
		return TestingAll

	default:
		return TestingUnit
	}
}
