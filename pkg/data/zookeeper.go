package data

import (
	"os"
	"strings"
	"sync"
	"time"

	"github.com/samuel/go-zookeeper/zk"
)

var (
	cacheZKConn sync.Once
	zkConn      *zk.Conn
)

// GetZK retrieves a cached ZooKeeper connection.
// Connection parameters are based on the environment
// variable ZOOKEEPER_SERVERS.
func GetZK() (*zk.Conn, error) {
	var err error
	cacheZKConn.Do(func() {
		addrs := strings.Split(os.Getenv("ZOOKEEPER_SERVERS"), ",")
		conn, _, e := zk.Connect(addrs, time.Second*5)

		zkConn = conn
		err = e
	})
	return zkConn, err
}

// CreateZNodePath creates a persistent znode for each part in the path
// example path: /bot/liveness
func CreateZNodePath(path string, conn *zk.Conn) error {
	node := "/"
	path += "/" // helps us grab the last znode
	for i := 1; i < len(path); i++ {
		if path[i] == '/' {
			exists, _, err := conn.Exists(node)
			if err != nil {
				return nil
			}

			if !exists {
				_, err := conn.Create(node, nil, 0, zk.WorldACL(zk.PermAll))
				if err != nil {
					return err
				}
			}
		}
		node += string(path[i])
	}
	return nil
}

// DeleteZNodeRecursive deletes a node and all of its descendents.
func DeleteZNodeRecursive(path string, conn *zk.Conn) error {
	exists, _, err := conn.Exists(path)
	if err != nil {
		return err
	}
	if !exists {
		return nil
	}

	children, _, err := conn.Children(path)
	if err != nil {
		return err
	}

	if len(children) != 0 {
		for _, childPath := range children {
			err := DeleteZNodeRecursive(path+"/"+childPath, conn)
			if err != nil {
				return err
			}
		}
	}
	return conn.Delete(path, 0)
}
